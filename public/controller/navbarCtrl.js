 'use strict';

 var app = angular.module('ices');

 app.controller('AuthCtrl', function($rootScope, AccountService,  UserService, $location, $modal, $http, $scope, $log) {

  $scope.dataLoading = 'false';

  $scope.currentUser = Parse.User.current();

  $rootScope.auth = Parse.auth;
  $scope.user = {};
  $scope.errorMessage = null;

   $scope.login_modal = function (size) {

        var modalInstance = $modal.open({
          templateUrl: 'views/sign-in.html',
          controller: $scope.loginCtrl,
          size: size,

        });


        modalInstance.result.then(function (selectedItem) {
          $scope.selected = selectedItem;
          }, function () {
            $log.info('Modal dismissed at: ' + new Date());
          });
  };

  return $scope.loginCtrl = function($scope, $rootScope, $modalInstance, $window,  $timeout, $location){

     $scope.ok = function () {
        $modalInstance.dismiss('cancel');
     };

     $scope.signin = function(user) {
      $scope.dataLoading = 'true';
        if (!(user.username && user.password)) {
          return $scope.errorMessage = 'Please supply a username and password';
        }

        Parse.User.logIn(user.username, user.password, {
          success: function(user) {

            $scope.currentUser = user;
            $scope.$apply();

            $modalInstance.dismiss('cancel');
            return $location.path("/home");

          },
          error: function(user, error) {
            return $scope.errorMessage = error.message;
          }
        });
      };
     $scope.signup = function (size) {

      var modalInstance = $modal.open({
        templateUrl: 'views/register.html',
        controller: $scope.signupCtrl,
        size: size,

      });

      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
      }, function () {
          $log.info('Modal dismissed at: ' + new Date());
        });

      };

      $scope.signupCtrl = function($scope, $rootScope, $modalInstance, $http, $window, $timeout, $location) {

        $scope.ok = function () {
          $modalInstance.dismiss('cancel');
        };

        $scope.register = function(user) {
            if (user.password !== user.passwordConfirm) {
              return $scope.errorMessage = "Passwords must match";
            }
            if (!(user.username && user.password)) {
              return $scope.errorMessage = 'Please supply a username and password';
            }

            $scope.user.role = 'viewer';
            delete $scope.user.passwordConfirm;

            return UserService.getUserExist(user)
            .then(function(data){
              var accountresult = data;
              $scope.dataLoading = 'true';
                $modalInstance.dismiss('cancel');
            }, function(error) {
              return $scope.errorMessage = err.data.error;
            });

          };

      }


      $scope.signout = function() {
        Parse.User.logOut();
        var currentUser = Parse.User.current();
        $location.path('/');
        window.location.reload();
      };

 }

});
