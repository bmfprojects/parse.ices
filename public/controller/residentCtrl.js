'use strict';

var app = angular.module('ices');

app.controller('residentCtrl', function(
  $scope, RegionServices, ResidentServices,
  PlaceServices,  ItemsServices,ClearanceService, $window,
  $rootScope, allPurposeFunctions, $modal, $http, $q, $filter, $location){

  $scope.currentPage = 1;
  $scope.pageSize = 6;
  $scope.residents = [];

    function loadAllResident() {
        ResidentServices.GetAllResindet()
          .success(function(data){
               return $scope.residents = data.results;
          });
    }

    loadAllResident();

      $scope.residentModel = function (size) {
            var modalInstance = $modal.open({
              templateUrl: '../views/resident.html',
              controller: $scope.CreateResidents,
              size: size
            });

            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
              }, function () {
              });

       };

        $scope.GetResidentInfo = function (size, id) {

            var modalInstance = $modal.open({
              templateUrl: '../views/resident.html',
              controller: $scope.EditMode,
              size: size,
              resolve: {
                    getresident: function($http){
                        if(id){
                          return  ResidentServices.GetResident(id);
                        }else{
                          return null;
                        }
                      }
                    }
            });

            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
              }, function () {
              });

      };

      $scope.EditMode = function($scope, $modalInstance, getresident, $modal) {

             $scope.region_bplace = {};
             $scope.children = {};
             $scope.children_me = [];
             $scope.getregion = [];


            ResidentServices.GetAllResindet()
            .success(function(data){
                 $scope.children_me=data.results;
            });

            RegionServices.GetAllRegion()
                .success(function(data){
                     $scope.getregion = data.results;
                });

            $scope.$watch('brgyresident.region_bplace.objectId', function(id){
                  PlaceServices.province_items(id)
                  .then(function(places) {
                    $scope.getprovince = places;
                    }, function(error) {
                      alert('Failed: ' + error);
                    });
            });

            $scope.$watch('brgyresident.province_bplace.id', function(id){
                  PlaceServices.municipality_items(id)
                  .then(function(places) {
                     $scope.getmunicipalities = places;
                    }, function(error) {
                      alert('Failed: ' + error);
                    });
            });

            $scope.$watch('brgyresident.municipality_bplace.id', function(id){
                  PlaceServices.baranggay_items(id)
                  .then(function(places) {
                     $scope.getbaranggay = places;
                    }, function(error) {
                      alert('Failed: ' + error);
                    });
            });

            $scope.someFunction = function (item, model){
              $scope.counter++;
              $scope.eventResult = {item: item, model: model};
            };

            $scope.languagesfamily    = ItemsServices.familylanguage();
            $scope.languages              = ItemsServices.languages();
            $scope.languagesdialect   = ItemsServices.languagesdialect();
            $scope.immunizations       = ItemsServices.immunizations();
            $scope.nutritions               = ItemsServices.nutritions();
            $scope.healths                  = ItemsServices.healths();
            $scope.others                   = ItemsServices.others();
            $scope.waters                   = ItemsServices.waters();
            $scope.supplies                 = ItemsServices.supplies();
            $scope.facilities                 = ItemsServices.facilities();
            $scope.otherones               = ItemsServices.otherones();

           $scope.take_snapshot = function(data_uri) {
                    Webcam.snap( function(data_uri) {
                        $scope.brgyresident.imageuri = data_uri;
                        document.getElementById('capture_image').innerHTML =
                            '<img id="myImg" src="'+data_uri+'"/>' +
                            '<input type="hidden" id="resulturi" ng-model="brgyresident.imageuri" value="'
                            + $scope.brgyresident.imageuri +'" >';
                    });

                        var x = document.getElementById("myImg").src;
                        var x_result = x.toString();
           }

          $scope.brgyresident = getresident.data;

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.brgyresident = {};
          $scope.UpdateResidentInfo=function(id){
              return ResidentServices.UpdateResident(id, $scope.brgyresident)
                        .success(function(data){
                            loadAllResident();
                            $modalInstance.dismiss('cancel');
                            $scope.resident = data;
                        });
          };

          $scope.brgyresident = getresident.data;
      };

      $scope.CreateResidents = function($scope, $modalInstance, $modal) {

            $scope.brgyresident = {};
            $scope.children = {};
            $scope.region_bplace = {};

            $scope.children_me = [];
            $scope.getregion = [];

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel');
            };

            ResidentServices.GetAllResindet()
            .success(function(data){
                 $scope.children_me=data.results;
            });

            RegionServices.GetAllRegion()
            .success(function(data){
                $scope.getregion = data.results;
            });

          $scope.$watch('brgyresident.region_bplace.objectId', function(id){
                PlaceServices.province_items(id)
                .then(function(places) {
                  $scope.getprovince = places;
                  }, function(error) {
                    alert('Failed: ' + error);
                  });
          });

          $scope.$watch('brgyresident.province_bplace.id', function(id){
                PlaceServices.municipality_items(id)
                .then(function(places) {
                   $scope.getmunicipalities = places;
                  }, function(error) {
                    alert('Failed: ' + error);
                  });
          });

          $scope.$watch('brgyresident.municipality_bplace.id', function(id){
                PlaceServices.baranggay_items(id)
                .then(function(places) {
                   $scope.getbaranggay = places;
                  }, function(error) {
                    alert('Failed: ' + error);
                  });
          });

          $scope.someFunction = function (item, model){
              $scope.counter++;
              $scope.eventResult = {item: item, model: model};
            };

            $scope.languagesfamily    = ItemsServices.familylanguage();
            $scope.languages              = ItemsServices.languages();
            $scope.languagesdialect   = ItemsServices.languagesdialect();
            $scope.immunizations      = ItemsServices.immunizations();
            $scope.nutritions               = ItemsServices.nutritions();
            $scope.healths                  = ItemsServices.healths();
            $scope.others                   = ItemsServices.others();
            $scope.waters                   = ItemsServices.waters();
            $scope.supplies                 = ItemsServices.supplies();
            $scope.facilities                 = ItemsServices.facilities();
            $scope.otherones               = ItemsServices.otherones();

            $scope.take_snapshot = function(data_uri) {
                    Webcam.snap( function(data_uri) {
                        $scope.brgyresident.imageuri = data_uri;
                        document.getElementById('capture_image').innerHTML =
                            '<img id="myImg" src="'+data_uri+'"/>' +
                            '<input type="hidden" id="resulturi" ng-model="brgyresident.imageuri" value="'
                            + $scope.brgyresident.imageuri +'" >';
                    });

                        var x = document.getElementById("myImg").src;
                        var x_result = x.toString();
           }

          $scope.CreateNewResident=function(){
            console.log('saving action')

              return ResidentServices.CreateResident($scope.brgyresident)
              .success(function(data){
                   loadAllResident();
                  $modalInstance.dismiss('cancel');
                  $scope.resident = data;
                       console.log($scope.brgyresident);
              });
          };
      };

      $scope.destroy = function (size, id, f, m, l) {

            var modalInstance = $modal.open({
              templateUrl: '../views/deleteresident.html',
              controller: $scope.deleteme,
              size: size,
               resolve: {
                      getdelete: function(){
                             var contentdata = {
                              id : id,
                              f : f,
                              m : m,
                              l : l
                             };
                             return contentdata;
                        }
                    }
            });

            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
              }, function () {
                 // $log.info('Modal dismissed at: ' + new Date());
                 // window.location.reload();
              });

      };


     $scope.deleteme = function($scope, $modalInstance, getdelete, $modal) {

          $scope.getdelete = getdelete;

          $scope.yes = function(id, f, m, l){
               return ResidentServices.DeleteResident(id)
               .success(function(){
                      loadAllResident();
                      $modalInstance.dismiss('cancel');
               });
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
      }


    $scope.viewclearance = function (size, id) {

        var modalInstance = $modal.open({
          templateUrl: '../views/viewresident.html',
          controller: $scope.ClearanceCtrl,
          size: size,
          resolve: {
                getresidentforclearance: function(){
                    if(id){
                      return  ResidentServices.GetResident(id);
                    }else{
                      return null;
                    }
                  }
                }
        });

        modalInstance.result.then(function (selectedItem) {
          $scope.selected = selectedItem;
          }, function () {
              window.location.reload();
          });

    };

    $scope.calculate_age = function(d){
          var birthday = new Date(d);
          var ageDifMs = Date.now() - birthday.getTime();
          var ageDate = new Date(ageDifMs);
          return Math.abs(ageDate.getFullYear() - 1970);
    };

  $scope.ClearanceCtrl = function($scope, $modalInstance, getresidentforclearance, $modal) {

   var url_value =  getresidentforclearance.data.objectId;

//Generate the QRCode for every full name of the resident
    var qrcode = new QRCode("qrcode", {
                     width : 200,
                     height : 200
                   });

    var sss = qrcode.makeCode(url_value);
    console.log(url_value);
    $scope.currentPageclearance = 1;
    $scope.pageSizeclearance = 6;
    $scope.clearancelist = [];

        function loadResidentClearance(){
        ClearanceService.ClearanceofResident(getresidentforclearance.data.objectId)
           .then(function(data){
                  return $scope.clearancelist = data;
            });
        }

       loadResidentClearance();

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };
      var resident = {};
      $scope.brgyresidentClearance = getresidentforclearance.data;
      resident = $scope.brgyresidentClearance;

        $scope.create_clearance = function (size, resident) {

        var modalInstance = $modal.open({
          templateUrl: '../views/clearance.html',
          controller: $scope.clearanceCtrl,
          size: size

        });

        modalInstance.result.then(function (selectedItem) {
          $scope.selected = selectedItem;
          }, function () {
            // $log.info('Modal dismissed at: ' + new Date());
          });

      };

      $scope.clearanceCtrl = function($scope, $modalInstance, $modal) {

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel');
            };

            $scope.purpose                  = ItemsServices.purpose();
            $scope.remark                   = ItemsServices.remark();

          $scope.createClearance = function(){

            $scope.clearance.residentID = getresidentforclearance.data.objectId;
              return ClearanceService.CreateClearance($scope.clearance)
              .success(function(data){
                loadResidentClearance();
                  $modalInstance.dismiss('cancel');
                  $scope.clearance = data;
              });
          }

      }
        $scope.edit_clearance = function (size, id) {

        var modalInstance = $modal.open({
          templateUrl: '../views/clearance.html',
          controller: $scope.editClearance,
          size: size,
          resolve:{
            getclearance:function(){
                  return ClearanceService.GetClearance(id);
            }
          }

        });

        modalInstance.result.then(function (selectedItem) {
          $scope.selected = selectedItem;
          }, function () {
            // $log.info('Modal dismissed at: ' + new Date());
          });

      };

      $scope.editClearance = function($scope, getclearance, $modalInstance, $modal) {
            $scope.clearance = getclearance.data;
            $scope.cancel = function () {
              $modalInstance.dismiss('cancel');
            };

            $scope.purpose                  = ItemsServices.purpose();
            $scope.remark                   = ItemsServices.remark();

          $scope.updateClearance = function(id){

            $scope.clearance.residentID = getresidentforclearance.data.objectId;
              return ClearanceService.UpdateClearance(id, $scope.clearance)
              .success(function(data){
                loadResidentClearance();
                  $modalInstance.dismiss('cancel');
                  $scope.clearance = data;
              });
          }

      }

        $scope.download_pdf = function(id){

            ClearanceService.GetClearance(id)
            .then(function(result){
              var dataresult = result.data;
              ResidentServices.GetResident(dataresult.residentID)
              .then(function(results){

                  var getresidentinfo = results.data,
                  todays = new Date(),
                  doc = new jsPDF('landscape'),
                  getperiodof_time = allPurposeFunctions.formatAMPM(todays),
                  prof_firstname = getresidentinfo.prof_firstname,
                  prof_middle_name = getresidentinfo.prof_middle_name,
                  prof_lastname = getresidentinfo.prof_lastname,
                  prof_gender = getresidentinfo.prof_gender,
                  prof_status = getresidentinfo.prof_status,
                  b_date = getresidentinfo.b_date,
                  remark = dataresult.remark,
                  purpose = dataresult.purpose,
                  date_of_issue = allPurposeFunctions.generatedata(dataresult.jud_issuedON),
                  getage = allPurposeFunctions.generateage(getresidentinfo.b_date),
                  currentTime = allPurposeFunctions.currentTime(),
                  DateToday = allPurposeFunctions.generatedata(todays),
                  Birthdate = allPurposeFunctions.generatedata(getresidentinfo.b_date),
                  idgetbaseme = getresidentinfo.imageuri,
                  idgetbase = document.getElementById('basegetme').src;

                    doc.setFontSize(12);
                    doc.text(120, 10, "Republic of the Philippines");
                    doc.text(120, 15, 'Municipality/City of ' );
                    doc.setFontSize(17);
                    doc.setFontType("bold");
                    doc.text(90, 25, 'Sangguniang Barangay ng ');

                    doc.setDrawColor(255,0,0);
                    doc.setLineWidth(1.5);
                    doc.line(20, 170, 280, 170);

                    doc.setDrawColor(0,0,0);
                    doc.setLineWidth(.5);
                    doc.line(215, 140, 275, 140);


                    doc.setDrawColor(0,0,0);
                    doc.setFontSize(17);
                    doc.setFont("helvetica");

                    doc.setFontType("bold");
                    doc.text(110, 35, 'BARANGAY CLEARANCE');

                    doc.setFontSize(11);
                    doc.setFontType("italic");
                    doc.text(35, 50, 'TO WHOM IT MAY CONCERN:');

                    doc.setFontSize(11);
                    doc.setFont("helvetica");
                    doc.text(50, 60, 'This is to certify that the person whose name, picture and signature appear hereon');
                    doc.text(35, 65, 'has requested a CERTIFICATION from this office and the result/s is/are listed below:');
                    doc.setFontSize(11);
                    doc.text(35, 75, 'NAME');
                    doc.text(35, 80, 'ADDRESS');
                    doc.text(35, 85, 'GENDER');
                    doc.text(35, 90, 'CIVIL STATUS');
                    doc.text(35, 95, 'BIRTH-DATE');
                    doc.text(35, 100, 'BIRTH-PLACE');
                    doc.text(35, 105, 'AGE');

                    doc.text(70, 75, ' : ');
                    doc.text(70, 80, ' : ');
                    doc.text(70, 85, ' : ');
                    doc.text(70, 90, ' : ');
                    doc.text(70, 95, ' : ');
                    doc.text(70, 100, ' : ');
                    doc.text(70, 105, ' : ');

                    doc.text(75, 75, prof_firstname + ' ' + prof_middle_name + ' ' + prof_lastname);
                    // doc.text(75, 80, barangay.barangay_name +', '+ municipality.municipality_name + ', '
                    //   + province.province_name);
                    doc.text(75, 85, prof_gender);
                    doc.text(75, 90, prof_status);
                    doc.text(75, 95, Birthdate);
                    // doc.text(75, 100, baranggay_bplace);
                    doc.text(75, 105, getage);

                    doc.setFont("helvetica")
                    doc.text(35, 115, 'Given this');
                    doc.text(35, 120, 'Remark');
                    doc.text(35, 125, 'Purpose');


                    doc.text(70, 115, ' : ');
                    doc.text(70, 120, ' : ');
                    doc.text(70, 125, ' : ');
                    doc.text(70, 130, ' : ');
                    doc.text(70, 135, ' : ');
                    doc.text(70, 140, ' : ');
                    doc.text(70, 145, ' : ');
                    doc.text(70, 145, ' : ');

                    doc.text(75, 115, DateToday);
                    doc.text(75, 120, remark);
                    doc.text(75, 125, purpose);
                    // doc.text(75, 130, clearance_control_no);
                    doc.text(75, 135, date_of_issue);
                    // doc.text(75, 140, barangay.barangay_name +', '+ municipality.municipality_name + ', '
                    //   + province.province_name);
                    doc.text(75, 145, getperiodof_time);

                    doc.text(220, 145, 'SIGNATURE OF APPLICANT');

                    doc.text(190, 50, 'CLEARANCE ID : '+ id);
                    doc.text(35, 130, 'Tax Cert. Serial');
                    doc.text(35, 135, 'Issued on');
                    doc.text(35, 140, 'Issued at');
                    doc.text(35, 145, 'Time');

                    doc.setFontType('bolditalic');
                    // doc.text(140, 160, captain);
                    doc.setFontType('italic');
                    doc.text(140, 165, 'PUNONG BARANGAY');
                    doc.text(35, 175, 'Note: Not valid without official dry seal. This Barangay Clearance is valid for 1 year from date of issue.');

                    doc.addImage(idgetbaseme, 'JPEG', 220, 55, 50, 50);
                    doc.addImage(idgetbase, 'PNG', 239, 172, 20, 20);

                    doc.save(prof_firstname +'.pdf');

              });
            });

        }
    }    

    $scope.generatethisID = function(id){

      var doc = new jsPDF();
      var get_ID_Format5  = 'data:image/jpg;base64,'+ItemsServices.id_format6();

      doc.setFont("helvetica");
      doc.setFontType("bold");
      doc.setFontSize(6);
      doc.setTextColor(0);

      console.log(id);

      ResidentServices.GetResident(id)
      .then(function(data){

        var getresidentid;
        var getresidentID = data.data;
        console.log(getresidentID);
        // var mlogo = $rootScope.UserAcount.mlogo;

                  // use bdate, if none given, default date is set
                  getresidentID.b_date = (getresidentID.b_date) ? getresidentID.b_date : "2015-05-13T16:00:00.000Z";
                  var formatDate = new Date(getresidentID.b_date);
                  var month = ["January","February","March","April","May","June","July","August","September","October","November","December"];
                  var month = month[formatDate.getMonth()];
                  var day = formatDate.getDate();
                  var year = formatDate.getFullYear();

                  var fullDate = month + " " + day + ", " + year;

                  // use region, province, municipality id, if none give, default value is set
                  var regionId = (getresidentID.region_bplace) ? getresidentID.region_bplace.objectId : "y4QYYdOJC6";
                  var provinceId = (getresidentID.province_bplace) ? getresidentID.province_bplace.id : "pwkuVPye1d";
                  var municipalityId = (getresidentID.municipality_bplace) ? getresidentID.municipality_bplace.id : "Fa0ZF0bVrp";
                  var objectId = getresidentID.objectId;

                  // set generated ID as combination of 4 ID's (this is temporary since there is no barangay_bplace in ResidentsCollection yet)
                  var generatedId = regionId + "-" + provinceId + "-" + municipalityId + "-" + objectId;

                  // set barangay and municipality name, if none given, default value is set
                  var barangayName = (getresidentID.barangay_bplace) ? getresidentID.barangay_bplace.barangay_name : "Cebu";
                  var municipalityName = (getresidentID.municipality_bplace) ? getresidentID.municipality_bplace.municipality_name : "Cebu City";

                  // set middlename, if none given, null value is set
                  var middleName = (getresidentID.prof_middle_name) ? getresidentID.prof_middle_name : "";

                  var captain = (getresidentID.captain) ? getresidentID.captain : "Jose Lopez";

                    getresidentid = {
                            generatedId : generatedId,
                            birthString: fullDate,
                            captain: captain,
                            imageuri: getresidentID.imageuri,
                            prof_gender: getresidentID.prof_gender,
                            firstname: getresidentID.prof_firstname,
                            middlename: middleName,
                            lastname: getresidentID.prof_lastname,
                            barangay_name: barangayName,
                            municipality: municipalityName,
                            // province: getresidentID.barangay.province,
                            // mlogo: mlogo,
                            idqrcode : qr.toDataURL(generatedId)
                    };


                          // ID wrapper .. 1st 2 numbers is X and Y (position).. last 2 numbers is for the img size..
                          doc.addImage(get_ID_Format5, 'JPG', 17, 20, 87, 53);

                          doc.text(40, 26, "Republic of the Philippines");
                          doc.text(40, 29, 'Municipality/City of ' + getresidentid.municipality);
                          doc.text(40, 32, 'Sangguniang Barangay ng ' + getresidentid.barangay_name);
                          doc.addImage(getresidentid.idqrcode, 'JPEG', 85, 39, 17, 17);
                          doc.addImage(getresidentid.imageuri, 'JPEG', 60, 38, 21, 17);

                          // doc.addImage(getresidentid.mlogo, 'JPEG', 25, 24, 9, 9);
                          // doc.text(79, 63, 'ID: '+getresidentid.generatedId);
                          doc.text(45, 63, getresidentid.captain);
                          doc.text(42, 63, ' : ');
                          doc.text(25, 63, 'Brgy. Captain');
                          doc.setTextColor(255, 0, 0);
                          doc.text(25, 66, 'ID: '+getresidentid.generatedId);
                          doc.setTextColor(0);

                          doc.text(23, 41, ' Firstname ');
                          doc.text(23, 44, ' Middlename ');
                          doc.text(23, 47, ' Lastname ');
                          doc.text(23, 50, ' Gender ');
                          doc.text(23, 53, ' Birthdate ');

                          doc.text(36, 41, ' : ');
                          doc.text(36, 44, ' : ');
                          doc.text(36, 47, ' : ');
                          doc.text(36, 50, ' : ');
                          doc.text(36, 53, ' : ');

                          doc.text(39, 41, getresidentid.firstname);
                          doc.text(39, 44, getresidentid.middlename);
                          doc.text(39, 47, getresidentid.lastname);
                          doc.text(39, 50, getresidentid.prof_gender);
                          doc.text(39, 53, getresidentid.birthString);

                          doc.save(getresidentid.firstname+'_ID.pdf');

      });

    };    

});
