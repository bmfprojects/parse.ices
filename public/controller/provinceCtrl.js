'use strict';

var app = angular.module('ices');

app.controller('provinceCtrl', function($scope, ProvinceServices, PlaceServices, getregion, $window, $rootScope, $modal, $http, $q, $filter, $location){

    var regionId = [];
    $scope.provinces =[];
    regionId = getregion.data;
    $scope.ListHeading = regionId.region_name;

    function getProvinces() {
          PlaceServices.province_items(regionId.objectId)
        .then(function(places) {
           return $scope.provinces = places;
          }, function(error) {
            alert('Failed: ' + error);
          });
    }

    getProvinces();

    $scope.currentPage = 1;
    $scope.pageSize = 6;

      $scope.addprovince = function (size) {
            var modalInstance = $modal.open({
              templateUrl: '../views/provinces/province.html',
              controller: $scope.CreateProvince,
              size: size
            });

            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
              }, function () {
                  // window.location.reload();
              });
       };

        $scope.GetProvinceInfo = function (size, id) {

            var modalInstance = $modal.open({
              templateUrl: '../views/provinces/province.html',
              controller: $scope.EditMode,
              size: size,
              resolve: {
                    getprovince: function($http){
                        if(id){
                          return  ProvinceServices.GetProvince(id);
                        }else{
                          return null;
                        }
                      }
                    }
            });

            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
              }, function () {
                 // window.location.reload();
              });

      };

      $scope.EditMode = function($scope, $modalInstance, getprovince, $modal, $http, $rootScope, $timeout,  $route, $q, $location, $filter) {

          $scope.ProvinceHeading  = getregion.data;
          $scope.province = getprovince.data;

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.region = {};
          $scope.UpdateRegionInfo=function(id){

              $scope.rid =  regionId.objectId;
              $scope.province.region_Id = regionId;
              $scope.province.regiongetid =  $scope.rid;

              return ProvinceServices.UpdateProvince(id, $scope.province)
              .success(function(data){
                 getProvinces();
                  $modalInstance.dismiss('cancel');
                  $scope.province = data;
                  $location.path('/viewprovince/'+regionId.objectId);
              });
          };
          $scope.province = getprovince.data;
      };

      $scope.CreateProvince = function($scope, $modalInstance, $modal, $http, $rootScope, $timeout,  $route, $q, $location, $filter) {

          $scope.ProvinceHeading  = getregion.data;

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.province = {};
          $scope.CreateNewProvince=function(){

            $scope.rid =  regionId.objectId;
            $scope.province.region_Id = regionId;
            $scope.province.regiongetid =  $scope.rid;

              return ProvinceServices.CreateProvince($scope.province)
              .success(function(data){
                 getProvinces();
                  $modalInstance.dismiss('cancel');
                  $scope.province = data;
                  console.log($scope.province);
                  $location.path('/viewprovince/'+regionId.objectId);
              });
          };
      };

      $scope.destroy = function (size, id) {

            var modalInstance = $modal.open({
              templateUrl: '../views/provinces/deleteprovince.html',
              controller: $scope.deleteme,
              size: size,
               resolve: {
                      getdelete: function(){
                             var contentdata = {
                              id : id
                             };
                             return contentdata;
                        }
                      }
            });

            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
              }, function () {
                // $log.info('Modal dismissed at: ' + new Date());
                 // window.location.reload();
              });

      };


     $scope.deleteme = function($scope, $modalInstance, getdelete, $modal, $http, $rootScope, $timeout,  $route, $q, $location, $filter) {

          $scope.getdelete = getdelete;

          $scope.yes = function(id){
               return ProvinceServices.DeleteProvince(id)
               .success(function(){
                      getProvinces();
                      $modalInstance.dismiss('cancel');
               });

            };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
      }
});
