'use strict';

var app = angular.module('ices');

app.controller('baranggayCtrl', function($scope, $window, BaranggayServices, PlaceServices,  getmunicipal, $rootScope, $modal, $http, $q, $filter, $location){

    var municipalId = [];
    $scope.baranggaylist =[];
    municipalId = getmunicipal.data;
    $scope.ListHeading = municipalId.municipality_name;

    function getBaranggaylist(){
          PlaceServices.baranggay_items(municipalId.objectId)
        .then(function(places) {
          return  $scope.baranggaylist = places;
          }, function(error) {
            alert('Failed: ' + error);
          });
    }
getBaranggaylist();
    $scope.currentPage = 1;
    $scope.pageSize = 6;

      $scope.addmunicipal = function (size) {
            var modalInstance = $modal.open({
              templateUrl: '../views/baranggay/baranggay.html',
              controller: $scope.CreateBaranggay,
              size: size
            });

            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
              }, function () {
                  // window.location.reload();
              });
       };

        $scope.GetMunicipalInfo = function (size, id) {

                var modalInstance = $modal.open({
                  templateUrl: '../views/baranggay/baranggay.html',
                  controller: $scope.EditMode,
                  size: size,
                  resolve: {
                        getbaranggay: function($http){
                            if(id){
                              return  BaranggayServices.GetBaranggay(id);
                            }else{
                              return null;
                            }
                          }
                        }
                });

                modalInstance.result.then(function (selectedItem) {
                  $scope.selected = selectedItem;
                  }, function () {
                     // window.location.reload();
                  });

          };

$scope.EditMode = function($scope, $modalInstance, getbaranggay, $modal, $http, $rootScope, $timeout,  $route, $q, $location, $filter) {

          $scope.BaranggayHeading  = getmunicipal.data;
          $scope.baranggay = getbaranggay.data;

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
          console.log(getmunicipal);
          $scope.region = {};
          $scope.UpdateBaranggayInfo=function(id){

            $scope.baranggay.municipal_Id = municipalId;
            $scope.baranggay.municipalgetid = municipalId.objectId;
            $scope.baranggay.municipal_zip_code = municipalId.zip_code;

              return BaranggayServices.UpdateBaranggay(id, $scope.baranggay)
              .success(function(data){
                getBaranggaylist();
                  $modalInstance.dismiss('cancel');
                  $scope.baranggay = data;
                  $location.path('/baranggaylist/'+municipalId.objectId);
              });
          };

          $scope.baranggay = getbaranggay.data;
      };

      $scope.CreateBaranggay = function($scope, $modalInstance, $modal, $http, $rootScope, $timeout,  $route, $q, $location, $filter) {

          $scope.BaranggayHeading  = getmunicipal.data;

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.province = {};
          $scope.CreateNewBaranggay=function(){

            $scope.baranggay.municipal_Id = municipalId;
            $scope.baranggay.municipalgetid = municipalId.objectId;
            $scope.baranggay.municipal_zip_code = municipalId.zip_code;

              return BaranggayServices.CreateBaranggay($scope.baranggay)
              .success(function(data){
                getBaranggaylist();
                  $modalInstance.dismiss('cancel');
                  $scope.baranggay = data;
                  console.log($scope.baranggay);
                  $location.path('/baranggaylist/'+municipalId.objectId);
              });
          };
      };

      $scope.destroy = function (size, id) {

            var modalInstance = $modal.open({
              templateUrl: '../views/municipal/deletemunicipal.html',
              controller: $scope.deleteme,
              size: size,
               resolve: {
                      getdelete: function(){
                             var contentdata = {
                              id : id
                             };
                             return contentdata;
                        }
                      }
            });

            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
              }, function () {
                // $log.info('Modal dismissed at: ' + new Date());
                 // window.location.reload();
              });

      };


     $scope.deleteme = function($scope, $modalInstance, getdelete, $modal, $http, $rootScope, $timeout,  $route, $q, $location, $filter) {

          $scope.getdelete = getdelete;

          $scope.yes = function(id){
               return MunicipalServices.DeleteMunicipal(id)
               .success(function(){
                      getBaranggaylist();
                      $modalInstance.dismiss('cancel');
               });

            };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
      }

});