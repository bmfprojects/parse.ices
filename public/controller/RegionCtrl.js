
'use strict';

var app = angular.module('ices');

app.controller('regionCtrl', function($scope, RegionServices, $window, $rootScope, $modal, $http, $q, $filter, $location){

    $scope.ListHeading = 'Region Records';
    $scope.currentPage = 1;
    $scope.pageSize = 6;
    $scope.regions = [];

    function getregions(){
        RegionServices.GetAllRegion()
        .success(function(data){
             return $scope.regions = data.results;
        });
    }

    getregions();

      $scope.addregion = function (size) {
            var modalInstance = $modal.open({
              templateUrl: '../views/regions/region.html',
              controller: $scope.CreateRegion,
              size: size
            });

            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
              }, function () {
                  // window.location.reload();
              });
       };

        $scope.GetRegionInfo = function (size, id) {

            var modalInstance = $modal.open({
              templateUrl: '../views/regions/region.html',
              controller: $scope.EditMode,
              size: size,
              resolve: {
                    getregion: function($http){
                        if(id){
                          return  RegionServices.GetRegion(id);
                        }else{
                          return null;
                        }
                      }
                    }
            });

            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
              }, function () {
                 // window.location.reload();
              });

      };

      $scope.EditMode = function($scope, $modalInstance, getregion, $modal, $http, $rootScope, $timeout,  $route, $q, $location, $filter) {

          $scope.region = getregion.data;

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
          console.log(getregion);
          $scope.region = {};
          $scope.UpdateRegionInfo=function(id){
              return RegionServices.UpdateRegion(id, $scope.region)
              .success(function(data){
                 getregions();
                  $modalInstance.dismiss('cancel');

                  $scope.region = data;
              });
          };
          $scope.region = getregion.data;
      };

      $scope.CreateRegion = function($scope, $modalInstance, $modal, $http, $rootScope, $timeout,  $route, $q, $location, $filter) {
          $scope.regionHeading ='Region Form';
          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.region = {};

          $scope.CreateNewRegion=function(){
              return RegionServices.CreateRegion($scope.region)
              .success(function(data){
                  getregions();
                  $modalInstance.dismiss('cancel');
                  $scope.region = data;
                  $location.path('/regionlist');
              });
          };
      };

      $scope.destroy = function (size, id) {

            var modalInstance = $modal.open({
              templateUrl: '../views/regions/deleteregion.html',
              controller: $scope.deleteme,
              size: size,
               resolve: {
                      getdelete: function(){
                             var contentdata = {
                              id : id
                             };
                             return contentdata;
                        }
                      }
            });

            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
              }, function () {
                // $log.info('Modal dismissed at: ' + new Date());
                 // window.location.reload();
              });

      };


     $scope.deleteme = function($scope, $modalInstance, getdelete, $modal, $http, $rootScope, $timeout,  $route, $q, $location, $filter) {

          $scope.getdelete = getdelete;

          $scope.yes = function(id){
               return RegionServices.DeleteRegion(id)
               .success(function(){
                     getregions();
                      $modalInstance.dismiss('cancel');
               });

            };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
      }
});

