'use strict';

var app = angular.module('ices');

app.controller('judicialCtrl', function($scope, JudicialServices, ResidentServices, RegionServices,  PlaceServices, ItemsServices, $window, $rootScope, $modal, $http, $q, $filter, $location){

  $scope.currentPage = 1;
  $scope.pageSize = 6;
  $scope.judicials = [];

    function loadAllJudicial() {
            JudicialServices.GetAllJudicial()
            .success(function(data){
            $scope.judicials=data.results;
            console.log($scope.judicials);
              });
            }
            loadAllJudicial();


      $scope.judicialModel = function (size) {
            var modalInstance = $modal.open({
              templateUrl: '../views/judicial.html',
              controller: $scope.CreateJudicials,
              size: size
            });

            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
              }, function () {
                  ///window.location.reload();
              });

       };
       $scope.GetJudicialInfo = function (size, id) {

            var modalInstance = $modal.open({
              templateUrl: '../views/judicial.html',
              controller: $scope.EditMode,
              size: size,
              resolve: {
                    getjudicial: function($http){
                        if(id){
                          return  JudicialServices.GetJudicial(id);
                        }else{
                          return null;
                        }
                      }
                    }
            });

            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
              }, function () {
               //  window.location.reload();
              });

      };

          $scope.judicial_view = function (size, id) {

          var modalInstance = $modal.open({
            templateUrl: '../views/viewjudicial.html',
            controller: $scope.judicial_editmodel,
            size: size,
             resolve: {
                    getjudicial: function($http){
                        if(id){
                           return  JudicialServices.GetJudicial(id);
                        }else{
                          return null;

                        }
                      },

                    }
          });


          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
              // $log.info('Modal dismissed at: ' + new Date());
            });

    };



$scope.judicial_editmodel = function($scope, $modalInstance, getjudicial, $modal, $http, $rootScope, $timeout,  $route, $q, $location, $filter) {

            $scope.brgyresident = {};
            $scope.children = {};
            $scope.region_bplace = {};

            $scope.children_me = [];
            $scope.getregion = [];
            $scope.complainant_resident = [];
            $scope.respondent_resident = [];

            $scope.judicial_me = [];
            ResidentServices.GetAllResindet()
            .success(function(data){
                 $scope.respondent_resident=data.results;

            });
            ResidentServices.GetAllResindet()
            .success(function(data){
                 $scope.complainant_resident=data.results;

            });

            RegionServices.GetAllRegion()
                .success(function(data){
                     $scope.getregion = data.results;

                });

            $scope.$watch('brgyjudicial.region_bplace.objectId', function(id){
                  PlaceServices.province_items(id)
                  .then(function(places) {
                    $scope.getprovince = places;
                    }, function(error) {
                      alert('Failed: ' + error);
                    });
            });

            $scope.$watch('brgyjudicial.province_bplace.id', function(id){
                  PlaceServices.municipality_items(id)
                  .then(function(places) {
                     $scope.getmunicipalities = places;
                    }, function(error) {
                      alert('Failed: ' + error);
                    });
            });

            $scope.$watch('brgyjudicial.municipality_bplace.id', function(id){
                  PlaceServices.baranggay_items(id)
                  .then(function(places) {
                     $scope.getbaranggay = places;
                    }, function(error) {
                      alert('Failed: ' + error);
                    });
            });

            $scope.someFunction = function (item, model){
              $scope.counter++;
              $scope.eventResult = {item: item, model: model};
            };
   $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };


          $scope.brgyjudicial = {};

          $scope.GetJudicial=function(id){
              return JudicialServices.GetJudicial(id, $scope.brgyjudicial)
                        .success(function(data){
                            loadAllJudicial();
                            $modalInstance.dismiss('cancel');
                            $scope.judicial = data;
                        });
          };
          $scope.brgyjudicial = getjudicial.data;


};

      $scope.EditMode = function($scope, $modalInstance, getjudicial, $modal, $http, $rootScope, $timeout,  $route, $q, $location, $filter) {

            $scope.brgyresident = {};
            $scope.children = {};
            $scope.region_bplace = {};

            $scope.children_me = [];
            $scope.getregion = [];
            $scope.complainant_resident = [];
            $scope.respondent_resident = [];

            $scope.judicial_me = [];
            ResidentServices.GetAllResindet()
            .success(function(data){
                 $scope.respondent_resident=data.results;

            });
            ResidentServices.GetAllResindet()
            .success(function(data){
                 $scope.complainant_resident=data.results;

            });

            RegionServices.GetAllRegion()
                .success(function(data){
                     $scope.getregion = data.results;

                });

            $scope.$watch('brgyjudicial.region_bplace.objectId', function(id){
                  PlaceServices.province_items(id)
                  .then(function(places) {
                    $scope.getprovince = places;
                    }, function(error) {
                      alert('Failed: ' + error);
                    });
            });

            $scope.$watch('brgyjudicial.province_bplace.id', function(id){
                  PlaceServices.municipality_items(id)
                  .then(function(places) {
                     $scope.getmunicipalities = places;
                    }, function(error) {
                      alert('Failed: ' + error);
                    });
            });

            $scope.$watch('brgyjudicial.municipality_bplace.id', function(id){
                  PlaceServices.baranggay_items(id)
                  .then(function(places) {
                     $scope.getbaranggay = places;
                    }, function(error) {
                      alert('Failed: ' + error);
                    });
            });

            $scope.someFunction = function (item, model){
              $scope.counter++;
              $scope.eventResult = {item: item, model: model};
            };

            $scope.languagesfamily      = ItemsServices.familylanguage();
            $scope.languages                = ItemsServices.languages();
            $scope.languagesdialect     = ItemsServices.languagesdialect();
            $scope.immunizations        = ItemsServices.immunizations();
            $scope.nutritions                = ItemsServices.nutritions();
            $scope.healths                   = ItemsServices.healths();
            $scope.others                    = ItemsServices.others();
            $scope.waters                   = ItemsServices.waters();
            $scope.supplies                 = ItemsServices.supplies();
            $scope.facilities                 = ItemsServices.facilities();
            $scope.otherones               = ItemsServices.otherones();
            $scope.incomes                = ItemsServices.incomes();
            $scope.cottages               = ItemsServices.cottages();
            $scope.OEAs                     = ItemsServices.OEAs();
            $scope.problems             = ItemsServices.problems();

  $scope.brgyjudicial = getjudicial.data;

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.brgyjudicial = {};
          $scope.UpdateJudicialInfo=function(id){
              return JudicialServices.UpdateJudicial(id, $scope.brgyjudicial)
                        .success(function(data){
                            loadAllJudicial();
                            $modalInstance.dismiss('cancel');
                            $scope.judicial = data;
                        });
          };
          $scope.brgyjudicial = getjudicial.data;
      };

$scope.CreateJudicials = function($scope, $modalInstance, $modal, $http, $rootScope, $timeout,  $route, $q, $location, $filter) {
            $scope.brgyresident = {};
            $scope.children = {};
            $scope.region_bplace = {};

            $scope.children_me = [];
            $scope.getregion = [];
            $scope.complainant_resident = [];
            $scope.respondent_resident = [];

            $scope.judicial_me = [];
            ResidentServices.GetAllResindet()
            .success(function(data){
                 $scope.respondent_resident=data.results;

            });
            ResidentServices.GetAllResindet()
            .success(function(data){
                 $scope.complainant_resident=data.results;

            });


            $scope.cancel = function () {
              $modalInstance.dismiss('cancel');
            };

            ResidentServices.GetAllResindet()
            .success(function(data){
                 $scope.children_me=data.results;
            });

            RegionServices.GetAllRegion()
            .success(function(data){
                $scope.getregion = data.results;
            });

          $scope.$watch('brgyresident.region_bplace.objectId', function(id){
                PlaceServices.province_items(id)
                .then(function(places) {
                  $scope.getprovince = places;
                  }, function(error) {
                    alert('Failed: ' + error);
                  });
          });

          $scope.$watch('brgyresident.province_bplace.id', function(id){
                PlaceServices.municipality_items(id)
                .then(function(places) {
                   $scope.getmunicipalities = places;
                  }, function(error) {
                    alert('Failed: ' + error);
                  });
          });

          $scope.$watch('brgyresident.municipality_bplace.id', function(id){
                PlaceServices.baranggay_items(id)
                .then(function(places) {
                   $scope.getbaranggay = places;
                  }, function(error) {
                    alert('Failed: ' + error);
                  });
          });

          $scope.someFunction = function (item, model){
              $scope.counter++;
              $scope.eventResult = {item: item, model: model};
            };
            $scope.languagesfamily    = ItemsServices.familylanguage();
            $scope.languages              = ItemsServices.languages();
            $scope.languagesdialect   = ItemsServices.languagesdialect();
            $scope.immunizations      = ItemsServices.immunizations();
            $scope.nutritions               = ItemsServices.nutritions();
            $scope.healths                  = ItemsServices.healths();
            $scope.others                   = ItemsServices.others();
            $scope.waters                   = ItemsServices.waters();
            $scope.supplies                 = ItemsServices.supplies();
            $scope.facilities                 = ItemsServices.facilities();
            $scope.otherones               = ItemsServices.otherones();
            $scope.incomes                = ItemsServices.incomes();
            $scope.cottages               = ItemsServices.cottages();
            $scope.OEAs                     = ItemsServices.OEAs();
            $scope.problems             = ItemsServices.problems();

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.brgyjudicial = {};

          $scope.CreateNewJudicial=function(){
            $scope.brgyjudicial.respondent;
            var respondentInfo = $scope.brgyjudicial.respondent.split(',');
            var getId_respondent = respondentInfo[4];
            $scope.brgyjudicial.respondentId = getId_respondent;
              return JudicialServices.CreateJudicial($scope.brgyjudicial)
              .success(function(data){
                loadAllJudicial();
                  $modalInstance.dismiss('cancel');
                  $scope.judicial = data;
                       console.log($scope.brgyjudicial);
              });
          };
      };


      $scope.destroy = function (size, id, f, m, l,c) {

            var modalInstance = $modal.open({
              templateUrl: '../views/deletejudicial.html',
              controller: $scope.deleteme,
              size: size,
               resolve: {
                      getdelete: function(){
                             var contentdata = {
                              id : id,
                              f : f,
                              m : m,
                              l : l,
                              c:c
                             };
                             return contentdata;
                        }
                      }
            });

            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
              }, function () {
                // $log.info('Modal dismissed at: ' + new Date());
                 window.location.reload();
              });

      };


     $scope.deleteme = function($scope, $modalInstance, getdelete, $modal, $http, $rootScope, $timeout,  $route, $q, $location, $filter) {

          $scope.getdelete = getdelete;

          $scope.yes = function(id, f, m, l,c){
               return JudicialServices.DeleteJudicial(id)
               .success(function(){
                      $modalInstance.dismiss('cancel');
               });

            };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
      }
});
