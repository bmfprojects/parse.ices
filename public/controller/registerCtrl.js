'use strict';

var app = angular.module('ices');

app.controller('registerCtrl', function(
  $scope, $window, $rootScope,  
  $modal, $log, $http, $q, $filter, 
  $location, $timeout, RegionServices, ProvinceServices) {

    $scope.currentUser = Parse.User.current();

    $scope.getAllUsers = function() {

      var province_userlist = Parse.Object.extend("province_userlist");
      var query = new Parse.Query(province_userlist);

      query.find({
        success: function(results) {
          $scope.province_account = results;
          console.log($scope.province_account);
        },
        error: function(object, error) {
          $scope.error = error.data.message;
        }
      });

      console.log($scope.currentUser);

    };

    $scope.getAllUsers();

    $scope.addNewUser = function (size) {

      var modalInstance = $modal.open({

      templateUrl: '../views/provinces/register.html',
      controller: $scope.addUserCtrl,
      size: size

      });
      
      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
        }, function () {
          // $log.info('Modal dismissed at: ' + new Date());
           // window.location.reload();
        });

    };

    $scope.addUserCtrl = function($scope, $modalInstance, $modal, $http, $rootScope, $timeout, $route, $q, $location, $filter) {

          $scope.modalLabel = 'Create Account';

          RegionServices.GetAllRegion()
          .success(function(data){
              $scope.getregion = data.results;
              console.log($scope.getregion);
          });

          $scope.$watch('account.region.objectId', function(id){
              ProvinceServices.GetProvinceByRegion(id)
              .then(function(data){
                  $scope.getprovince = data;
                  console.log($scope.getprovince);
              });
          });

          $scope.save = function () {

            $scope.currentUser = Parse.User.current();

            if($scope.currentUser.attributes.accesscontrol === 'regionadmin') {

              $scope.account.adminObjectId = $scope.currentUser.id;
              $scope.account.adminUsername = $scope.currentUser.attributes.username;
              $scope.account.adminRole = $scope.currentUser.attributes.role;
              $scope.account.isActive = "Active";
              $scope.account.activeCaption = "Deactivate";

              if($scope.account.pswrd != $scope.account.verifypassword) {
                $scope.error = "Passwords do not match!";
              }

              else {

                var province_userlist = Parse.Object.extend("province_userlist");
                var provinceAccount = new province_userlist();

                provinceAccount.set("username", $scope.account.username);
                provinceAccount.set("password", $scope.account.password);
                provinceAccount.set("themes", $scope.account.themes);
                provinceAccount.set("accesscontrol", $scope.account.accesscontrol);
                provinceAccount.set("tnc", $scope.account.tnc);
                provinceAccount.set("region", $scope.account.region);
                provinceAccount.set("province", $scope.account.province);
                provinceAccount.set("createdById", $scope.account.adminObjectId);
                provinceAccount.set("updateby", $scope.account.adminUsername);
                provinceAccount.set("createdbyadmin", $scope.account.adminUsername);
                provinceAccount.set("role", $scope.account.adminRole);
                provinceAccount.set("active", $scope.account.isActive);
                provinceAccount.set("active_caption", $scope.account.activeCaption);

                provinceAccount.save(null, {
                  success: function(result) {
                      $scope.account = result.data;
                      $modalInstance.dismiss('cancel');
                      window.location.reload();
                  },
                  error: function(result, error) {
                    $scope.error = error.data.message;
                    // console.log($scope.account);
                  }
                });
              }
            }  
          };
      
          $scope.ok = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

    };

    $scope.deactivate = function(id, active, access){

      var province_userlist = Parse.Object.extend("province_userlist");
      var query = new Parse.Query(province_userlist);

      if(active == 'Active'){

        query.get(id, {
          success: function(object) {

            $scope.account = {
              isActive : 'Deactivated',
              activeCaption: 'Activate',
              access : access
            };

            object.set("active", $scope.account.isActive);
            object.set("active_caption", $scope.account.activeCaption);
            object.set("accesscontrol", $scope.account.access);

            object.save(null, {
              success: function(result) {
                window.location.reload();
              },
              error: function(result, error) {
                $scope.error = error.data.message;
              }
            });

          }
        });

      }

      else if(active == 'Deactivated'){

        query.get(id, {
          success: function(object) {

            $scope.account = {
              isActive : 'Active',
              activeCaption: 'Deactivate',
              access : access
            };

            object.set("active", $scope.account.isActive);
            object.set("active_caption", $scope.account.activeCaption);
            object.set("accesscontrol", $scope.account.access);

            object.save(null, {
              success: function(result) {
                window.location.reload();
              },
              error: function(result, error) {
                $scope.error = error.data.message;
              }
            });
            
          }
          
        });

      }

    }

    $scope.editProvinceAccount = function (size, id) {

      var province_userlist = Parse.Object.extend("province_userlist");
      var query = new Parse.Query(province_userlist);

      query.get(id)
        .then(function(object) {
          var modalInstance = $modal.open({
            templateUrl: '../views/provinces/register.html',
            controller: $scope.editProvinceAccountCtrl,
            size: size,
            resolve: {
                getuser: function(){
                  if(id){
                    return object;
                  }else{
                    return null;
                  }
                }
              }
          });

          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
              // $log.info('Modal dismissed at: ' + new Date());
              // window.location.reload();
            });    
        });

    };

    $scope.editProvinceAccountCtrl = function($scope, $modalInstance, $modal, getuser, $http, $rootScope, $timeout, $route, $q, $location, $filter) {

      $scope.modalLabel = 'Edit Account';

      console.log(getuser.attributes);

      $scope.account = {
        username: getuser.attributes.username,
        themes: getuser.attributes.themes,
        region: getuser.attributes.region,
        province: getuser.attributes.province,
        accesscontrol: getuser.attributes.accesscontrol,
        createdById: getuser.attributes.createdById,
      };

      RegionServices.GetAllRegion()
        .success(function(data){
            $scope.getregion = data.results;
            console.log($scope.getregion);
        });

        $scope.$watch('account.region.objectId', function(id){
            ProvinceServices.GetProvinceByRegion(id)
            .then(function(data){
                $scope.getprovince = data;
                console.log($scope.getprovince);
            });
        });

      $scope.update = function () {

        $scope.currentUser = Parse.User.current();

        if($scope.currentUser.attributes.accesscontrol === 'regionadmin') {

          $scope.account.adminObjectId = $scope.currentUser.id;
          $scope.account.adminUsername = $scope.currentUser.attributes.username;
          $scope.account.adminRole = $scope.currentUser.attributes.role;
          $scope.account.isActive = "Active";
          $scope.account.activeCaption = "Deactivate";

          if($scope.account.pswrd != $scope.account.verifypassword) {
            $scope.error = "Passwords do not match!";
          }

          else {
            getuser.set("username", $scope.account.username);
            getuser.set("themes", $scope.account.themes);
            getuser.set("region", $scope.account.region);
            getuser.set("province", $scope.account.province);
            getuser.set("createdById", $scope.account.adminObjectId);
            getuser.set("updateby", $scope.account.adminUsername);
            getuser.set("createdbyadmin", $scope.account.adminUsername);
            getuser.set("role", $scope.account.adminRole);
            getuser.set("active", $scope.account.isActive);
            getuser.set("active_caption", $scope.account.activeCaption);

            getuser.save(null, {
              success: function(result) {
                window.location.reload();
              },
              error: function(result, error) {
                $scope.error = error.data.message;
              }
            });
          }
        }  
      };

      $scope.ok = function () {
        $modalInstance.dismiss('cancel');
      };

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };

      $scope.caption_pass = 'Change Password';

      $scope.enable_new = function (){
        $scope.newpass = 'true';
        $scope.caption_pass = 'Click to hide';
      }

      $scope.enable_hide = function (){
        $scope.newpass = 'false';
        $scope.caption_pass = 'Change Password';
      }
    
    } 

});