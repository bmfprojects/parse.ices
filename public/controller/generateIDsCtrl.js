'use strict';

var app = angular.module('ices');

app.controller('generateIDsCtrl', function($scope, ItemsServices,
  $window, $rootScope, $location, $modal, BaranggayServices, ResidentServices, $http, $q, $filter){

    // check if the UserAccount exists, if not clear the sessionStorage

       // $http.get(DbCollection + 'account/')
       //  .then(function(result){
       //    $rootScope.UserAcount = result.data;
       //    if($rootScope.UserAcount == null || $window.sessionStorage["userInfo"] == null){
       //      sessionStorage.clear();
       //      $location.path('/login');
       //      window.location.reload();
       //    }
       //  });

        var get_ID_Format5  = 'data:image/jpg;base64,'+ItemsServices.id_format6();

        ResidentServices.GetAllResindet()
         .success(function(data){

            $scope.residents = data.results;
            console.log($scope.residents.length);
            console.log($scope.residents);

            // var mlogo = $rootScope.UserAcount.mlogo

                        var ID_res = [];
                         for (var i = 0; i < $scope.residents.length; i++) {
                             var results = $scope.residents[i];

                                // use bdate, if none given, default date is set
                                results.b_date = (results.b_date) ? results.b_date : "2015-05-13T16:00:00.000Z";
                                var formatDate = new Date(results.b_date);
                                var month = ["January","February","March","April","May","June","July","August","September","October","November","December"];
                                var month = month[formatDate.getMonth()];
                                var day = formatDate.getDate();
                                var year = formatDate.getFullYear();

                                var fullDate = month + " " + day + ", " + year;

                                // use region, province, municipality id, if none give, default value is set
                                var regionId = (results.region_bplace) ? results.region_bplace.objectId : "y4QYYdOJC6";
                                var provinceId = (results.province_bplace) ? results.province_bplace.id : "pwkuVPye1d";
                                var municipalityId = (results.municipality_bplace) ? results.municipality_bplace.id : "Fa0ZF0bVrp";
                                var objectId = results.objectId;

                                // set generated ID as combination of 4 ID's (this is temporary since there is no barangay_bplace in ResidentsCollection yet)
                                var generatedId = regionId + "-" + provinceId + "-" + municipalityId + "-" + objectId;

                                // set barangay and municipality name, if none given, default value is set
                                var barangayName = (results.barangay_bplace) ? results.barangay_bplace.barangay_name : "Cebu";
                                var municipalityName = (results.municipality_bplace) ? results.municipality_bplace.municipality_name : "Cebu City";

                                // set middlename, if none given, null value is set
                                var middleName = (results.prof_middle_name) ? results.prof_middle_name : "";

                                var captain = (results.captain) ? results.captain : "Jose Lopez"


                                  ID_res.push({

                                      generatedId : generatedId,
                                      birthString: fullDate,
                                      captain: captain,
                                      imageuri: results.imageuri,
                                      prof_gender: results.prof_gender,
                                      firstname: results.prof_firstname,
                                      middlename: middleName,
                                      lastname: results.prof_lastname,
                                      barangay_name: barangayName,
                                      municipality: municipalityName,
                                      // province: data.province_bplace.province_name,
                                      // mlogo: mlogo,
                                      idqrcode : qr.toDataURL(generatedId)

                                    });

                                    $scope.printID = function(){

                                          var doc = new jsPDF();

                                          for (var i = 0; i < $scope.residents.length; i++) {

                                            doc.setFont("helvetica");
                                            doc.setFontType("bold");
                                            doc.setFontSize(6);
                                           
                                            switch(i % 8) {

                                              case 0:

                                                // ID wrapper .. 1st 2 numbers is X and Y (position).. last 2 numbers is for the img size..
                                                doc.addImage(get_ID_Format5, 'JPG', 17, 20, 87, 53);


                                                doc.setTextColor(0);

                                                doc.text(40, 26, "Republic of the Philippines");
                                                doc.text(40, 29, 'Municipality/City of ' + ID_res[i].municipality);
                                                doc.text(40, 32, 'Sangguniang Barangay ng ' + ID_res[i].barangay_name);
                                                doc.addImage(ID_res[i].idqrcode, 'JPEG', 85, 39, 17, 17);
                                                doc.addImage(ID_res[i].imageuri, 'PNG', 60, 38, 21, 17);

                                                // doc.addImage(ID_res[i].mlogo, 'JPEG', 25, 24, 9, 9);
                                                // doc.text(79, 63, 'ID: '+ID_res[i].generatedId);
                                                doc.text(45, 62, ID_res[i].captain);
                                                doc.text(42, 62, ' : ');
                                                doc.text(25, 62, 'Brgy. Captain');
                                                doc.setTextColor(255, 0, 0);
                                                doc.text(25, 65, 'ID: '+ID_res[i].generatedId);
                                                doc.setTextColor(0);


                                                doc.text(23, 41, ' Firstname ');
                                                doc.text(23, 44, ' Middlename ');
                                                doc.text(23, 47, ' Lastname ');
                                                doc.text(23, 50, ' Gender ');
                                                doc.text(23, 53, ' Birthdate ');

                                                doc.text(36, 41, ' : ');
                                                doc.text(36, 44, ' : ');
                                                doc.text(36, 47, ' : ');
                                                doc.text(36, 50, ' : ');
                                                doc.text(36, 53, ' : ');

                                                doc.text(39, 41, ID_res[i].firstname);
                                                doc.text(39, 44, ID_res[i].middlename);
                                                doc.text(39, 47, ID_res[i].lastname);
                                                doc.text(39, 50, ID_res[i].prof_gender);
                                                doc.text(39, 53, ID_res[i].birthString);


                                                break;
                                        

                                              case 1:

                                                doc.addImage(get_ID_Format5, 'JPG', 110, 20, 87, 53);

                                                doc.setTextColor(0);
                                                doc.text(133, 26, "Republic of the Philippines");
                                                doc.text(133, 29, 'Municipality/City of ' + ID_res[i].municipality);
                                                doc.text(133, 32, 'Sangguniang Barangay ng ' + ID_res[i].barangay_name);
                                                doc.addImage(ID_res[i].idqrcode, 'PNG', 178, 39, 17, 17);
                                                doc.addImage(ID_res[i].imageuri, 'JPEG', 153, 38, 21, 17);

                                                // doc.addImage(ID_res[i].mlogo, 'JPEG', 118, 24, 9, 9);
                                                // doc.text(172, 63, 'ID: '+ID_res[i].generatedId);
                                                doc.text(138, 62, ID_res[i].captain);
                                                doc.text(135, 62, ' : ');
                                                doc.text(118, 62, ' Brgy. Captain ');
                                                doc.setTextColor(255, 0, 0);
                                                doc.text(118, 65, 'ID: '+ID_res[i].generatedId);
                                                doc.setTextColor(0);


                                                doc.text(116, 41, ' Firstname ');
                                                doc.text(116, 44, ' Middlename ');
                                                doc.text(116, 47, ' Lastname ');
                                                doc.text(116, 50, ' Gender ');
                                                doc.text(116, 53, ' Birthdate ');

                                                doc.text(129, 41, ' : ');
                                                doc.text(129, 44, ' : ');
                                                doc.text(129, 47, ' : ');
                                                doc.text(129, 50, ' : ');
                                                doc.text(129, 53, ' : ');

                                                doc.text(132, 41, ID_res[i].firstname);
                                                doc.text(132, 44, ID_res[i].middlename);
                                                doc.text(132, 47, ID_res[i].lastname);
                                                doc.text(132, 50, ID_res[i].prof_gender);
                                                doc.text(132, 53, ID_res[i].birthString);

                                                break;

                                              case 2:

                                                doc.addImage(get_ID_Format5, 'JPG', 17, 75, 87, 53);

                                                doc.setTextColor(0);
                                                doc.text(40, 81, "Republic of the Philippines");
                                                doc.text(40, 84, 'Municipality/City of ' + ID_res[i].municipality);
                                                doc.text(40, 87, 'Sangguniang Barangay ng ' + ID_res[i].barangay_name);
                                                doc.addImage(ID_res[i].idqrcode, 'PNG', 85, 94, 17, 17);
                                                doc.addImage(ID_res[i].imageuri, 'JPEG', 60, 93, 21, 17);

                                                // doc.addImage(ID_res[i].mlogo, 'JPEG', 25, 79, 9, 9);
                                                // doc.text(79, 118, 'ID: '+ID_res[i].generatedId);
                                                doc.text(45, 118, ID_res[i].captain);
                                                doc.text(42, 118, ' : ');
                                                doc.text(25, 118, ' Brgy. Captain ');
                                                doc.setTextColor(255, 0, 0);
                                                doc.text(25, 121, 'ID: '+ID_res[i].generatedId);
                                                doc.setTextColor(0);

                                                doc.text(23, 96, ' Firstname ');
                                                doc.text(23, 99, ' Middlename ');
                                                doc.text(23, 102, ' Lastname ');
                                                doc.text(23, 105, ' Gender ');
                                                doc.text(23, 108, ' Birthdate ');

                                                doc.text(36, 96, ' : ');
                                                doc.text(36, 99, ' : ');
                                                doc.text(36, 102, ' : ');
                                                doc.text(36, 105, ' : ');
                                                doc.text(36, 108, ' : ');

                                                doc.text(39, 96, ID_res[i].firstname);
                                                doc.text(39, 99, ID_res[i].middlename);
                                                doc.text(39, 102, ID_res[i].lastname);
                                                doc.text(39, 105, ID_res[i].prof_gender);
                                                doc.text(39, 108, ID_res[i].birthString);

                                                break;

                                              case 3:

                                                doc.addImage(get_ID_Format5, 'JPG', 110, 75, 87, 53);

                                                doc.setTextColor(0);
                                                doc.text(133, 81, "Republic of the Philippines");
                                                doc.text(133, 84, 'Municipality/City of ' + ID_res[i].municipality);
                                                doc.text(133, 87, 'Sangguniang Barangay ng ' + ID_res[i].barangay_name);

                                                doc.addImage(ID_res[i].idqrcode, 'PNG', 178, 94, 17, 17);
                                                doc.addImage(ID_res[i].imageuri, 'JPEG', 153, 93, 21, 17);

                                                // doc.addImage(ID_res[i].mlogo, 'JPEG', 118, 79, 9, 9);
                                                // doc.text(172, 118, 'ID: '+ID_res[i].generatedId);
                                                doc.text(138, 118, ID_res[i].captain);
                                                doc.text(135, 118, ' : ');
                                                doc.text(118, 118, ' Brgy. Captain ');
                                                doc.setTextColor(255, 0, 0);
                                                doc.text(118, 121, 'ID: '+ID_res[i].generatedId);
                                                doc.setTextColor(0);

                                                doc.text(116, 96, ' Firstname ');
                                                doc.text(116, 99, ' Middlename ');
                                                doc.text(116, 102, ' Lastname ');
                                                doc.text(116, 105, ' Gender ');
                                                doc.text(116, 108, ' Birthdate ');

                                                doc.text(129, 96, ' : ');
                                                doc.text(129, 99, ' : ');
                                                doc.text(129, 102, ' : ');
                                                doc.text(129, 105, ' : ');
                                                doc.text(129, 108, ' : ');

                                                doc.text(132, 96, ID_res[i].firstname);
                                                doc.text(132, 99, ID_res[i].middlename);
                                                doc.text(132, 102, ID_res[i].lastname);
                                                doc.text(132, 105, ID_res[i].prof_gender);
                                                doc.text(132, 108, ID_res[i].birthString);

                                                break;

                                              case 4:

                                                doc.addImage(get_ID_Format5, 'JPG', 17, 130, 87, 53);

                                                doc.setTextColor(0);
                                                doc.text(40, 136, "Republic of the Philippines");
                                                doc.text(40, 139, 'Municipality/City of ' + ID_res[i].municipality);
                                                doc.text(40, 142, 'Sangguniang Barangay ng ' + ID_res[i].barangay_name);

                                                doc.addImage(ID_res[i].idqrcode, 'PNG', 85, 149, 17, 17);
                                                doc.addImage(ID_res[i].imageuri, 'JPEG', 60, 148, 21, 17);

                                                // doc.addImage(ID_res[i].mlogo, 'JPEG', 25, 134, 9, 9);
                                                // doc.text(79, 173, 'ID: '+ID_res[i].generatedId);
                                                doc.text(45, 173, ID_res[i].captain);
                                                doc.text(42, 173, ' : ');
                                                doc.text(25, 173, ' Brgy. Captain ');
                                                doc.setTextColor(255, 0, 0);
                                                doc.text(25, 176, 'ID: '+ID_res[i].generatedId);
                                                doc.setTextColor(0);

                                                doc.text(23, 151, ' Firstname ');
                                                doc.text(23, 154, ' Middlename ');
                                                doc.text(23, 157, ' Lastname ');
                                                doc.text(23, 160, ' Gender ');
                                                doc.text(23, 163, ' Birthdate ');

                                                doc.text(36, 151, ' : ');
                                                doc.text(36, 154, ' : ');
                                                doc.text(36, 157, ' : ');
                                                doc.text(36, 160, ' : ');
                                                doc.text(36, 163, ' : ');

                                                doc.text(39, 151, ID_res[i].firstname);
                                                doc.text(39, 154, ID_res[i].middlename);
                                                doc.text(39, 157, ID_res[i].lastname);
                                                doc.text(39, 160, ID_res[i].prof_gender);
                                                doc.text(39, 163, ID_res[i].birthString);

                                                break;

                                              case 5:

                                                doc.addImage(get_ID_Format5, 'JPG', 110, 130, 87, 53);

                                                doc.setTextColor(0);
                                                doc.text(133, 136, "Republic of the Philippines");
                                                doc.text(133, 139, 'Municipality/City of ' + ID_res[i].municipality);
                                                doc.text(133, 142, 'Sangguniang Barangay ng ' + ID_res[i].barangay_name);

                                                doc.addImage(ID_res[i].idqrcode, 'PNG', 178, 149, 17, 17);
                                                doc.addImage(ID_res[i].imageuri, 'JPEG', 153, 148, 21, 17);

                                                // doc.addImage(ID_res[i].mlogo, 'JPEG', 118, 134, 9, 9);
                                                // doc.text(172, 173, 'ID: '+ID_res[i].generatedId);
                                                doc.text(138, 173, ID_res[i].captain);
                                                doc.text(135, 173, ' : ');
                                                doc.text(118, 173, ' Brgy. Captain ');
                                                doc.setTextColor(255, 0, 0);
                                                doc.text(118, 176, 'ID: '+ID_res[i].generatedId);
                                                doc.setTextColor(0);

                                                doc.text(116, 151, ' Firstname ');
                                                doc.text(116, 154, ' Middlename ');
                                                doc.text(116, 157, ' Lastname ');
                                                doc.text(116, 160, ' Gender ');
                                                doc.text(116, 163, ' Birthdate ');

                                                doc.text(129, 151, ' : ');
                                                doc.text(129, 154, ' : ');
                                                doc.text(129, 157, ' : ');
                                                doc.text(129, 160, ' : ');
                                                doc.text(129, 163, ' : ');

                                                doc.text(132, 151, ID_res[i].firstname);
                                                doc.text(132, 154, ID_res[i].middlename);
                                                doc.text(132, 157, ID_res[i].lastname);
                                                doc.text(132, 160, ID_res[i].prof_gender);
                                                doc.text(132, 163, ID_res[i].birthString);

                                                break;

                                              case 6:

                                                doc.addImage(get_ID_Format5, 'JPG', 17, 185, 87, 53);

                                                doc.setTextColor(0);
                                                doc.text(40, 191, "Republic of the Philippines");
                                                doc.text(40, 194, 'Municipality/City of ' + ID_res[i].municipality);
                                                doc.text(40, 197, 'Sangguniang Barangay ng ' + ID_res[i].barangay_name);

                                                doc.addImage(ID_res[i].idqrcode, 'PNG', 85, 204, 17, 17);
                                                doc.addImage(ID_res[i].imageuri, 'JPEG', 60, 203, 21, 17);

                                                // doc.addImage(ID_res[i].mlogo, 'JPEG', 25, 189, 9, 9);
                                                // doc.text(79, 228, 'ID: '+ID_res[i].generatedId);
                                                doc.text(45, 228, ID_res[i].captain);
                                                doc.text(42, 228, ' : ');
                                                doc.text(25, 228, ' Brgy. Captain ');
                                                doc.setTextColor(255, 0, 0);
                                                doc.text(25, 231, 'ID: '+ID_res[i].generatedId);
                                                doc.setTextColor(0);

                                                doc.text(23, 206, ' Firstname ');
                                                doc.text(23, 209, ' Middlename ');
                                                doc.text(23, 212, ' Lastname ');
                                                doc.text(23, 215, ' Gender ');
                                                doc.text(23, 218, ' Birthdate ');

                                                doc.text(36, 206, ' : ');
                                                doc.text(36, 209, ' : ');
                                                doc.text(36, 212, ' : ');
                                                doc.text(36, 215, ' : ');
                                                doc.text(36, 218, ' : ');

                                                doc.text(39, 206, ID_res[i].firstname);
                                                doc.text(39, 209, ID_res[i].middlename);
                                                doc.text(39, 212, ID_res[i].lastname);
                                                doc.text(39, 215, ID_res[i].prof_gender);
                                                doc.text(39, 218, ID_res[i].birthString);

                                                break;

                                              case 7:

                                                doc.addImage(get_ID_Format5, 'JPG', 110, 185, 87, 53);

                                                doc.setTextColor(0);
                                                doc.text(133, 191, "Republic of the Philippines");
                                                doc.text(133, 194, 'Municipality/City of ' + ID_res[i].municipality);
                                                doc.text(133, 197, 'Sangguniang Barangay ng ' + ID_res[i].barangay_name);

                                                doc.addImage(ID_res[i].idqrcode, 'PNG', 178, 204, 17, 17);
                                                doc.addImage(ID_res[i].imageuri, 'JPEG', 153, 203, 21, 17);

                                                // doc.addImage(ID_res[i].mlogo, 'JPEG', 118, 189, 9, 9);
                                                // doc.text(172, 228, 'ID: '+ID_res[i].generatedId);
                                                doc.text(138, 228, ID_res[i].captain);
                                                doc.text(135, 228, ' : ');
                                                doc.text(118, 228, ' Brgy. Captain ');
                                                doc.setTextColor(255, 0, 0);
                                                doc.text(118, 231, 'ID: '+ID_res[i].generatedId);
                                                doc.setTextColor(0);

                                                doc.text(116, 206, ' Firstname ');
                                                doc.text(116, 209, ' Middlename ');
                                                doc.text(116, 212, ' Lastname ');
                                                doc.text(116, 215, ' Gender ');
                                                doc.text(116, 218, ' Birthdate ');

                                                doc.text(129, 206, ' : ');
                                                doc.text(129, 209, ' : ');
                                                doc.text(129, 212, ' : ');
                                                doc.text(129, 215, ' : ');
                                                doc.text(129, 218, ' : ');

                                                doc.text(132, 206, ID_res[i].firstname);
                                                doc.text(132, 209, ID_res[i].middlename);
                                                doc.text(132, 212, ID_res[i].lastname);
                                                doc.text(132, 215, ID_res[i].prof_gender);
                                                doc.text(132, 218, ID_res[i].birthString);

                                                doc.addPage(); // add page every time pdf page has 8 ID's already

                                                break;
                                            }

                                          }

                                          // the actual saving of pdf file with file name ID
                                          doc.save('ID.pdf');

                                    };

                        }


         });


});
