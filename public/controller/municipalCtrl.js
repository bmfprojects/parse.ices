'use strict';

var app = angular.module('ices');

app.controller('municipalCtrl', function($scope, $window, MunicipalServices, PlaceServices,  getprovince, $rootScope, $modal, $http, $q, $filter, $location){

    var provinceId = [];
    $scope.municipalities =[];
    provinceId = getprovince.data;
    $scope.ListHeading = provinceId.province_name;

    function getMunicipalities(){
          PlaceServices.municipality_items(provinceId.objectId)
        .then(function(places) {
           return $scope.municipalities = places;
          }, function(error) {
            alert('Failed: ' + error);
          });
    }

    getMunicipalities();

    $scope.currentPage = 1;
    $scope.pageSize = 6;

      $scope.addmunicipal = function (size) {
            var modalInstance = $modal.open({
              templateUrl: '../views/municipal/municipal.html',
              controller: $scope.CreateMunicipal,
              size: size
            });

            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
              }, function () {
                  // window.location.reload();
              });
       };

        $scope.GetMunicipalInfo = function (size, id) {

                var modalInstance = $modal.open({
                  templateUrl: '../views/municipal/municipal.html',
                  controller: $scope.EditMode,
                  size: size,
                  resolve: {
                        getmunicipal: function($http){
                            if(id){
                              return  MunicipalServices.GetMunicipal(id);
                            }else{
                              return null;
                            }
                          }
                        }
                });

                modalInstance.result.then(function (selectedItem) {
                  $scope.selected = selectedItem;
                  }, function () {
                     // window.location.reload();
                  });

          };

$scope.EditMode = function($scope, $modalInstance, getmunicipal, $modal, $http, $rootScope, $timeout,  $route, $q, $location, $filter) {

          $scope.MunicipalHeading  = getprovince.data;
          $scope.municipality = getmunicipal.data;

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.UpdateMunicipalInfo=function(id){

            $scope.municipality.province_Id = provinceId;
            $scope.municipality.provincegetid = provinceId.objectId;

              return MunicipalServices.UpdateMunicipal(id, $scope.municipality)
              .success(function(data){
                 getMunicipalities();
                  $modalInstance.dismiss('cancel');
                  $scope.municipality = data;
                  $location.path('/municipalities/'+provinceId.objectId);
              });
          };

          $scope.municipality = getmunicipal.data;
      };

      $scope.CreateMunicipal = function($scope, $modalInstance, $modal, $http, $rootScope, $timeout,  $route, $q, $location, $filter) {
          $scope.MunicipalHeading  = getprovince.data;
          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.province = {};
          $scope.CreateNewMunicipal=function(){

            $scope.municipality.province_Id = provinceId;
            $scope.municipality.provincegetid = provinceId.objectId;

              return MunicipalServices.CreateMunicipal($scope.municipality)
              .success(function(data){
                 getMunicipalities();
                  $modalInstance.dismiss('cancel');
                  $scope.municipality = data;
                  console.log($scope.municipality);
                  $location.path('/municipalities/'+provinceId.objectId);
              });
          };
      };

      $scope.destroy = function (size, id) {

            var modalInstance = $modal.open({
              templateUrl: '../views/municipal/deletemunicipal.html',
              controller: $scope.deleteme,
              size: size,
               resolve: {
                      getdelete: function(){
                             var contentdata = {
                              id : id
                             };
                             return contentdata;
                        }
                      }
            });

            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
              }, function () {
                // $log.info('Modal dismissed at: ' + new Date());
                 window.location.reload();
              });

      };


     $scope.deleteme = function($scope, $modalInstance, getdelete, $modal, $http, $rootScope, $timeout,  $route, $q, $location, $filter) {

          $scope.getdelete = getdelete;

          $scope.yes = function(id){
             getMunicipalities();
               return MunicipalServices.DeleteMunicipal(id)
               .success(function(){
                      $modalInstance.dismiss('cancel');
               });

            };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
      }

});