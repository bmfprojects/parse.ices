var app = angular.module('ices');

app.filter('labelCase', function (){
  return function (input){
    input = input.replace(/([A-Z])/g, ' $1');
    return input[0].toUpperCase() + input.slice(1);
  };
});

app.filter('keyFilter', function (){

  return function (obj, query){

    var result = {};

    angular.forEach(obj, function (val, key){
      if(key !== query){
      result[key] = val;
      }
    });

    return result;
  };
});

app.filter('truncate', function () {

        return function (text, length, end) {
            if (isNaN(length))
                length = 10;

            if (end === undefined)
                end = "...";

            if (text.length <= length || text.length - end.length <= length) {
                return text;
            }
            else {
                return String(text).substring(0, length-end.length) + end;
            }

        };
 });

app.filter('camelCase', function () {

  return function(input){
    return input.toLowerCase().replace(/ (\w)/g, function (match, letter){
      return letter.toUpperCase();
    });
  }
});
app.filter('truncate', function () {
        return function (text, length, end) {
            if (isNaN(length))
                length = 10;

            if (end === undefined)
                end = "...";

            if (text.length <= length || text.length - end.length <= length) {
                return text;
            }
            else {
                return String(text).substring(0, length-end.length) + end;
            }

        };
});
app.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }
    return out;
  };
});