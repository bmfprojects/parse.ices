'use strict';

var app = angular.module('ices');

app.value('FieldTypes', {
  text: ['Text', 'should be a text'],
  email: ['Email', 'should be an email'],
  number: ['Number', 'should be a number'],
  date: ['Date', 'should be a date'],
  datetime: ['Datetime', 'should be a datetime'],
  month: ['Month', 'should be a Month'],
  week: ['Week', 'should be a week'],
  url: ['URL', 'should be a URL'],
  tel: ['Phone', 'should be a phon number'],
  color: ['Color', 'should be a color']});


app.directive('formField', function ($timeout, FieldTypes){

  return {

    restrict: 'EA',
    templateUrl: './views/form-field.html',
    replace: true,
    scope:{
      record: '=',
      field: '@',
      live: '@',
      required: '@'
    },
    link: function ($scope, element, attr){

      $scope.$on('record:invalid', function (){
        $scope[$scope.field].$setDirty();
      })
      $scope.type = FieldTypes;

      $scope.remove = function(field){
        delete $scope.record[field];
        $scope.blurUpdate();
      };

      $scope.blurUpdate = function () {
        if($scope.live !== 'false') {
          $scope.record.put(function (updateRecord){
            $scope.record = updateRecord;
          });
        }
      };

      var saveTimeout;
      $scope.update = function () {
        $timeout.cancel(saveTimeout);
        saveTimeout = $timeout($scope.blurUpdate, 1000);
      };
    }

  };
});
app.directive('newField', function ($filter, FieldTypes){

  return {
    restrict: 'EA',
    templateUrl: 'views/new-field.html',
    replace: true,
    scope:{
      record: "=",
      live: "@",
    },
    require: "^form",
    link: function ($scope, element, attr, form){
      $scope.types = FieldTypes;
      $scope.field = {};

      $scope.show = function(type){
        $scope.field.type = type;
        $scope.display = true;
      };

      $scope.remove = function(){
        $scope.field = {};
        $scope.display = false;
      };

      $scope.add = function(){
        if(form.newField.$valid){
          $scope.record[$filter('camelCase')($scope.field.name)] = [$scope.field.value, $scope.field.type];
          $scope.remove();
          if($scope.live !== 'false') {
            $scope.record.$update(function (updateRecord){
              $scope.record = updateRecord;
            });
          }
        }
      };

    }

  };

});
app.directive('imageCrop', function() {
    return {
      template: '<div class="ng-image-crop ng-image-crop--{{ shape }}" ng-style="moduleStyles"><section ng-style="sectionStyles" ng-show="step==1"><span class="btn btn-success fileinput-button"><i class="glyphicon glyphicon-plus"></i><span>Browse image</span><input type="file" class="image-crop-filepicker" /></span></section><section ng-style="sectionStyles" ng-show="step==2"><canvas class="cropping-canvas" width="{{ canvasWidth }}" height="{{ canvasHeight }}" ng-mousemove="onCanvasMouseMove($event)" ng-mousedown="onCanvasMouseDown($event)" ng-mouseup="onCanvasMouseUp($event)"></canvas><div ng-style="croppingGuideStyles" class="cropping-guide"></div><div class="zoom-handle" ng-mousemove="onHandleMouseMove($event)" ng-mousedown="onHandleMouseDown($event)" ng-mouseup="onHandleMouseUp($event)"><span>&larr; zoom &rarr;</span></div><button ng-click="crop()" class="btn btn-info">Crop</button></section><section ng-style="sectionStyles" class="section-final" ng-show="step==3"><img class="final-cropped-image" ng-src="{{ croppedDataUri }}" /></section></div>',
      replace: true,
      restrict: 'AE',
      scope: {
        width: '@',
        height: '@',
        shape: '@',
        result: '=',
        step: '='
      },
      link: function (scope, element, attributes) {
        scope.step = scope.step || 1;
        scope.shape = scope.shape || 'circle';
        scope.width = parseInt(scope.width, 10) || 300;
        scope.height = parseInt(scope.height, 10) || 300;

        scope.canvasWidth = scope.width + 100;
        scope.canvasHeight = scope.height + 100;

        var $input = element.find('input[type=file]');
        var $canvas = element.find('canvas')[0];
        var $handle = document.getElementsByClassName('zoom-handle')[0];
        var $finalImg = document.getElementsByClassName('final-cropped-image')[0];
        var $img = new Image();
        var fileReader = new FileReader();

        var maxLeft = 0, minLeft = 0, maxTop = 0, minTop = 0, imgLoaded = false, imgWidth = 0, imgHeight = 0;
        var currentX = 0, currentY = 0, dragging = false, startX = 0, startY = 0, zooming = false;
        var newWidth = imgWidth, newHeight = imgHeight;
        var targetX = 0, targetY = 0;
        var zoom = 1;
        var maxZoomGestureLength = 0;
        var maxZoomedInLevel = 0, maxZoomedOutLevel = 2;
        var minXPos = 0, maxXPos = 50, minYPos = 0, maxYPos = 50; // for dragging bounds

        var zoomWeight = .4;
        var ctx = $canvas.getContext('2d');
        var exif = null;
        var files = [];

        // ---------- INLINE STYLES ----------- //
        scope.moduleStyles = {
          width: (scope.width + 100) + 'px',
          height: (scope.height + 100) + 'px'
        };

        scope.sectionStyles = {
          width: (scope.width + 100) + 'px',
          height: (scope.height + 100) + 'px'
        };

        scope.croppingGuideStyles = {
          width: scope.width + 'px',
          height: scope.height + 'px',
          top: '50px',
          left: '50px'
        };

        // ---------- EVENT HANDLERS ---------- //
        fileReader.onload = function(e) {
          $img.src = this.result;
          scope.step = 2;
          scope.$apply();

          var byteString = atob(this.result.split(',')[1]);
          var binary = new BinaryFile(byteString, 0, byteString.length);
          exif = EXIF.readFromBinaryFile(binary);

        };

        function reset() {
          files = [];
          zoom = 1;
          ctx.clearRect(0, 0, $canvas.width, $canvas.height);
          document.getElementsByClassName('image-crop-filepicker')[0].value = null;
          $img.src = '';
        }

        element.on('change', function(e){
          files = e.target.files;
          fileReader.readAsDataURL(files[0]);
         });


        $img.onload = function() {
          ctx.drawImage($img, 0, 0);

          imgWidth = $img.width;
          imgHeight = $img.height;

          if (exif && exif.Orientation) {

            // change mobile orientation, if required
            switch(exif.Orientation){
              case 1:
                  // nothing
                  break;
              case 2:
                  // horizontal flip
                  ctx.translate(imgWidth, 0);
                  ctx.scale(-1, 1);
                  break;
              case 3:
                  // 180 rotate left
                  ctx.translate(imgWidth, imgHeight);
                  ctx.rotate(Math.PI);
                  break;
              case 4:
                  // vertical flip
                  ctx.translate(0, imgHeight);
                  ctx.scale(1, -1);
                  break;
              case 5:
                  // vertical flip + 90 rotate right
                  ctx.rotate(0.5 * Math.PI);
                  ctx.scale(1, -1);
                  break;
              case 6:
                  // 90 rotate right
                  ctx.rotate(0.5 * Math.PI);
                  ctx.translate(0, -imgHeight);
                  break;
              case 7:
                  // horizontal flip + 90 rotate right
                  ctx.rotate(0.5 * Math.PI);
                  ctx.translate(imgWidth, -imgHeight);
                  ctx.scale(-1, 1);
                  break;
              case 8:
                  // 90 rotate left
                  ctx.rotate(-0.5 * Math.PI);
                  ctx.translate(-imgWidth, 0);
                  break;
              default:
                  break;
            }
          }

          minLeft = (scope.width + 100) - this.width;
          minTop = (scope.height + 100) - this.height;
          newWidth = imgWidth;
          newHeight = imgHeight;

          // console.log('canvas width', $canvas.width);
          // console.log('image width', imgWidth);

          maxZoomedInLevel = ($canvas.width - 100) / imgWidth;
          // console.log('maxZoomedInLevel', maxZoomedInLevel);

          maxZoomGestureLength = to2Dp(Math.sqrt(Math.pow($canvas.width, 2) + Math.pow($canvas.height, 2)));
          // console.log('maxZoomGestureLength', maxZoomGestureLength);


          updateDragBounds();

        };

        // ---------- PRIVATE FUNCTIONS ---------- //
        function moveImage(x, y) {

          if ((x < minXPos) || (x > maxXPos) || (y < minYPos) || (y > maxYPos)) {
            // new position is out of bounds, would show gutter
            return;
          }
          targetX = x;
          targetY = y;
          ctx.clearRect(0, 0, $canvas.width, $canvas.height);
          ctx.drawImage($img, x, y, newWidth, newHeight);
        }

        function to2Dp(val) {
          return Math.round(val * 1000) / 1000;
        }

        function updateDragBounds() {
          // $img.width, $canvas.width, zoom

          minXPos = $canvas.width - ($img.width * zoom) - 50;
          minYPos = $canvas.height - ($img.height * zoom) - 50;

        }

        function zoomImage(val) {

          if (!val) {
            return;
          }


          var proposedZoomLevel = to2Dp(zoom + val);

          if ((proposedZoomLevel < maxZoomedInLevel) || (proposedZoomLevel > maxZoomedOutLevel)) {
            // image wont fill whole canvas
            // or image is too far zoomed in, it's gonna get pretty pixelated!
            return;
          }

          zoom = proposedZoomLevel;
          // console.log('zoom', zoom);

          updateDragBounds();

          //  do image position adjustments so we don't see any gutter
          if (proposedZoomLevel === maxZoomedInLevel) {
            // image fills canvas perfectly, let's center it
            ctx.clearRect(0, 0, $canvas.width, $canvas.height);
            ctx.drawImage($img, 0, 0, $canvas.width, $canvas.height);
            return;
          }

          newWidth = $img.width * zoom;
          newHeight = $img.height * zoom;

          var newXPos = currentX * zoom;
          var newYPos = currentY * zoom;

          // check if we've exposed the gutter
          if (newXPos < minXPos) {
            newXPos = minXPos;
          } else if (newXPos > maxXPos) {
            newXPos = maxXPos;
          }

          if (newYPos < minYPos) {
            newYPos = minYPos;
          } else if (newYPos > maxYPos) {
            newYPos = maxYPos;
          }

          // check if image is still going to fit the bounds of the box
          ctx.clearRect(0, 0, $canvas.width, $canvas.height);
          ctx.drawImage($img, newXPos, newYPos, newWidth, newHeight);
        }

        function calcZoomLevel(diffX, diffY) {

          var hyp = Math.sqrt( Math.pow(diffX, 2) + Math.pow(diffY, 2) );
          var zoomGestureRatio = to2Dp(hyp / maxZoomGestureLength);
          var newZoomDiff = to2Dp((maxZoomedOutLevel - maxZoomedInLevel) * zoomGestureRatio * zoomWeight);
          return diffX > 0 ? -newZoomDiff : newZoomDiff;
        }

        // ---------- SCOPE FUNCTIONS ---------- //

        $finalImg.onload = function() {
          var tempCanvas = document.createElement('canvas');
          tempCanvas.width = this.width - 100;
          tempCanvas.height = this.height - 100;
          tempCanvas.style.display = 'none';
          // console.log('tempCanvas.width', tempCanvas.width, tempCanvas.height);

          var tempCanvasContext = tempCanvas.getContext('2d');
          // console.log('tempCanvasContext', tempCanvasContext);
          tempCanvasContext.drawImage($finalImg, -50, -50);

          document.getElementsByClassName('section-final')[0].appendChild(tempCanvas);
          scope.result = tempCanvas.toDataURL("image/jpeg");
          scope.$apply();

          reset();

        };

        scope.crop = function() {
          scope.croppedDataUri = $canvas.toDataURL("image/jpeg");
          scope.step = 3;
        };

        scope.onCanvasMouseUp = function(e) {

          if (!dragging) {
            return;
          }

          e.preventDefault();
          e.stopPropagation(); // if event was on canvas, stop it propagating up

          startX = 0;
          startY = 0;
          dragging = false;
          currentX = targetX;
          currentY = targetY;

          removeBodyEventListener('mouseup', scope.onCanvasMouseUp);
          removeBodyEventListener('touchend', scope.onCanvasMouseUp);
          removeBodyEventListener('mousemove', scope.onCanvasMouseMove);
          removeBodyEventListener('touchmove', scope.onCanvasMouseMove);
        };

        $canvas.addEventListener('touchend', scope.onCanvasMouseUp, false);

        scope.onCanvasMouseDown = function(e) {
          startX = e.type === 'touchstart' ? e.changedTouches[0].clientX : e.clientX;
          startY = e.type === 'touchstart' ? e.changedTouches[0].clientY : e.clientY;
          zooming = false;
          dragging = true;

          addBodyEventListener('mouseup', scope.onCanvasMouseUp);
          addBodyEventListener('mousemove', scope.onCanvasMouseMove);
        };

        $canvas.addEventListener('touchstart', scope.onCanvasMouseDown, false);

        function addBodyEventListener(eventName, func) {
          document.documentElement.addEventListener(eventName, func, false);
        }

        function removeBodyEventListener(eventName, func) {
          document.documentElement.removeEventListener(eventName, func);
        }

        scope.onHandleMouseDown = function(e) {

          e.preventDefault();
          e.stopPropagation(); // if event was on handle, stop it propagating up

          startX = lastHandleX = (e.type === 'touchstart') ? e.changedTouches[0].clientX : e.clientX;
          startY = lastHandleY = (e.type === 'touchstart') ? e.changedTouches[0].clientY : e.clientY;
          dragging = false;
          zooming = true;

          addBodyEventListener('mouseup', scope.onHandleMouseUp);
          addBodyEventListener('touchend', scope.onHandleMouseUp);
          addBodyEventListener('mousemove', scope.onHandleMouseMove);
          addBodyEventListener('touchmove', scope.onHandleMouseMove);
        };

        $handle.addEventListener('touchstart', scope.onHandleMouseDown, false);

        scope.onHandleMouseUp = function(e) {

          // this is applied on the whole section so check we're zooming
          if (!zooming) {
            return;
          }

          e.preventDefault();
          e.stopPropagation(); // if event was on canvas, stop it propagating up

          startX = 0;
          startY = 0;
          zooming = false;
          currentX = targetX;
          currentY = targetY;

          removeBodyEventListener('mouseup', scope.onHandleMouseUp);
          removeBodyEventListener('touchend', scope.onHandleMouseUp);
          removeBodyEventListener('mousemove', scope.onHandleMouseMove);
          removeBodyEventListener('touchmove', scope.onHandleMouseMove);
        };

        $handle.addEventListener('touchend', scope.onHandleMouseUp, false);


        scope.onCanvasMouseMove = function(e) {

          e.preventDefault();
          e.stopPropagation();

          if (!dragging) {
            return;
          }



          var diffX = startX - ((e.type === 'touchmove') ? e.changedTouches[0].clientX : e.clientX); // how far mouse has moved in current drag
          var diffY = startY - ((e.type === 'touchmove') ? e.changedTouches[0].clientY : e.clientY); // how far mouse has moved in current drag
          /*targetX = currentX - diffX; // desired new X position
          targetY = currentY - diffY; // desired new X position*/

          moveImage(currentX - diffX, currentY - diffY);

        };

        $canvas.addEventListener('touchmove', scope.onCanvasMouseMove, false);


        var lastHandleX = null, lastHandleY = null;

        scope.onHandleMouseMove = function(e) {

          e.stopPropagation();
          e.preventDefault();

          // this is applied on the whole section so check we're zooming
          if (!zooming) {
            return false;
          }

          var diffX = lastHandleX - ((e.type === 'touchmove') ? e.changedTouches[0].clientX : e.clientX); // how far mouse has moved in current drag
          var diffY = lastHandleY - ((e.type === 'touchmove') ? e.changedTouches[0].clientY : e.clientY); // how far mouse has moved in current drag

          lastHandleX = (e.type === 'touchmove') ? e.changedTouches[0].clientX : e.clientX;
          lastHandleY = (e.type === 'touchmove') ? e.changedTouches[0].clientY : e.clientY;

          var zoomVal = calcZoomLevel(diffX, diffY);
          zoomImage(zoomVal);

        };

        $handle.addEventListener('touchmove', scope.onHandleMouseMove, false);

      }
    };
});
app.directive('imageUploader', [

    function imageUploader() {
      return {
        restrict: 'A',
        link : function(scope, elem, attr, ctrl) {
          var $imgDiv = $('.uploaded-image')
            , $elem
            , $status = elem.next('.progress')
            , $progressBar = $status.find('.bar')
            , config = {
                dataType : 'json',
                start : function(e) {
                  $elem = $(e.target);
                  $elem.hide();
                  $status.removeClass('hide');
                  $progressBar.text('Uploading...');
                },
                done : function(e, data) {
                  var url = data.result.url;
                  $('<img />').attr('src', url).appendTo($imgDiv.removeClass('hide'));
                  scope.$apply(function() {
                    scope.pick.photo = url;
                  })
                  console.log(scope);
                  console.log($status);
                  $status.removeClass('progress-striped progress-warning active').addClass('progress-success');
                  $progressBar.text('Done');
                },
                progress : function(e, data) {
                  var progress = parseInt(data.loaded / data.total * 100, 10);
                  $progressBar.css('width', progress + '%');
                  if (progress === 100) {
                    $status.addClass('progress-warning');
                    $progressBar.text('Processing...');
                  }
                },
                error : function(resp, er, msg) {
                  $elem.show();
                  $status.removeClass('active progress-warning progress-striped').addClass('progress-danger');
                  $progressBar.css('width', '100%');
                  if (resp.status === 415) {
                    $progressBar.text(msg);
                  } else {
                    $progressBar.text('There was an error. Please try again.');
                  }
                }
              };
          elem.fileupload(config);
        }
      }
    }
  ]);
app.directive('fileUpload', function () {
    return {
        scope: true,        //create a new scope
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var files = event.target.files;
                //iterate files since 'multiple' may be specified on the element
                for (var i = 0;i<files.length;i++) {
                    //emit event upward
                    scope.$emit("fileSelected", { file: files[i] });

                }
            });
        }
    };
})

app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                    console.log(files[i]);
                });
            });
        }
    };
}]);

app.service('fileUpload', ['$http', function ($scope, $http) {
    this.uploadFileToUrl = function(file, uploadUrl){

        var fd = new FormData();
        fd.append('file', file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(){
        })
        .error(function(){
        });
    }

}]);

app.directive('capitalizeFirst', function($parse) {
   return {
     require: 'ngModel',
     link: function(scope, element, attrs, modelCtrl) {
        var capitalize = function(inputValue) {
           if (inputValue === undefined) { inputValue = ''; }
           var capitalized = inputValue.charAt(0).toUpperCase() +
                             inputValue.substring(1);
           if(capitalized !== inputValue) {
              modelCtrl.$setViewValue(capitalized);
              modelCtrl.$render();
            }
            return capitalized;
         }
         modelCtrl.$parsers.push(capitalize);
         capitalize($parse(attrs.ngModel)(scope)); // capitalize initial value
     }
   };
});

app.directive("ngTimeSelector", function () {
    return {
        restrict: 'EA',
        template: '<div class="timeSelectorDirective"> <div class="section hours"> <div class="increase" ng-click="increaseHours()"> <i class="icon fa fa-caret-up"></i> </div> <div class="display"> {{displayHours()}} </div> <div class="decrease" ng-click="decreaseHours()"> <i class="icon fa fa-caret-down"></i> </div> </div> <div class="section minutes"> <div class="increase" ng-click="increaseMinutes()"> <i class="icon fa fa-caret-up"></i> </div> <div class="display"> {{displayMinutes()}} </div> <div class="decrease" ng-click="decreaseMinutes()"> <i class="icon fa fa-caret-down"></i> </div> </div> <div class="section hours"> <div class="increase" ng-click="switchPeriod()"> <i class="icon fa fa-caret-up"></i> </div> <div ng-if="hours >= 12" class="display"> PM </div> <div ng-if="hours < 12" class="display"> AM </div> <div class="decrease" ng-click="switchPeriod()"> <i class="icon fa fa-caret-down"></i> </div> </div> </div>',
        scope: {
            hours: "=",
            minutes: "="
        },
        replace: true,
        link: function (scope, elem, attr) {

            //Create vars
            scope.period = "AM";

            /* Increases hours by one */
            scope.increaseHours = function () {

                //Check whether hours have reached max
                if (scope.hours < 23) {
                    scope.hours = ++scope.hours;
                }
                else {
                    scope.hours = 0;
                }
            }

            /* Decreases hours by one */
            scope.decreaseHours = function () {

                //Check whether hours have reached min
                scope.hours = scope.hours <= 0 ? 23 : --scope.hours;
            }

            /* Increases minutes by one */
            scope.increaseMinutes = function () {

                //Check whether to reset
                if (scope.minutes >= 59) {
                    scope.minutes = 0;
                }
                else {
                    scope.minutes++;
                }
            }

            /* Decreases minutes by one */
            scope.decreaseMinutes = function () {

                //Check whether to reset
                if (scope.minutes <= 0) {
                    scope.minutes = 59;
                }
                else {
                    scope.minutes = --scope.minutes;
                }
            }


            /* Displays hours - what the user sees */
            scope.displayHours = function () {

                //Create vars
                var hoursToDisplay = scope.hours;

                //Check whether to reset etc
                if (scope.hours > 12) {
                    hoursToDisplay = scope.hours - 12;
                }

                //Check for 12 AM etc
                if (hoursToDisplay == 0) {

                    //Set to am and display 12
                    hoursToDisplay = 12;
                }
                else {

                    //Check whether to prepend 0
                    if (hoursToDisplay <= 9) {
                        hoursToDisplay = "0" + hoursToDisplay;
                    }
                }

                return hoursToDisplay;
            }

            /* Displays minutes */
            scope.displayMinutes = function () {
                return scope.minutes <= 9 ? "0" + scope.minutes : scope.minutes;
            }

            /* Switches the current period by ammending hours */
            scope.switchPeriod = function () {
                scope.hours = scope.hours >= 12 ? scope.hours - 12 : scope.hours + 12;
            }
        }
    }
});
app.directive('qrcode', function($window) {

    var canvas2D = !!$window.CanvasRenderingContext2D,
        levels = {
          'L': 'Low',
          'M': 'Medium',
          'Q': 'Quartile',
          'H': 'High'
        },
        draw = function(context, qr, modules, tile) {
          for (var row = 0; row < modules; row++) {
            for (var col = 0; col < modules; col++) {
              var w = (Math.ceil((col + 1) * tile) - Math.floor(col * tile)),
                  h = (Math.ceil((row + 1) * tile) - Math.floor(row * tile));

              context.fillStyle = qr.isDark(row, col) ? '#000' : '#fff';
              context.fillRect(Math.round(col * tile),
                               Math.round(row * tile), w, h);
            }
          }
        };

    return {
      restrict: 'E',
      template: '<canvas class="qrcode"></canvas>',
      link: function(scope, element, attrs) {
        var domElement = element[0],
            $canvas = element.find('canvas'),
            canvas = $canvas[0],
            context = canvas2D ? canvas.getContext('2d') : null,
            download = 'download' in attrs,
            href = attrs.href,
            link = download || href ? document.createElement('a') : '',
            trim = /^\s+|\s+$/g,
            error,
            version,
            errorCorrectionLevel,
            data,
            size,
            modules,
            tile,
            qr,
            $img,
            setVersion = function(value) {
              version = Math.max(1, Math.min(parseInt(value, 10), 10)) || 4;
            },
            setErrorCorrectionLevel = function(value) {
              errorCorrectionLevel = value in levels ? value : 'M';
            },
            setData = function(value) {
              if (!value) {
                return;
              }

              data = value.replace(trim, '');
              qr = qrcode(version, errorCorrectionLevel);
              qr.addData(data);

              try {
                qr.make();
              } catch(e) {
                error = e.message;
                return;
              }

              error = false;
              modules = qr.getModuleCount();
            },
            setSize = function(value) {
              size = parseInt(value, 10) || modules * 2;
              tile = size / modules;
              canvas.width = canvas.height = size;
            },
            render = function() {
              if (!qr) {
                return;
              }

              if (error) {
                if (link) {
                  link.removeAttribute('download');
                  link.title = '';
                  link.href = '#_';
                }
                if (!canvas2D) {
                  domElement.innerHTML = '<img src width="' + size + '"' +
                                         'height="' + size + '"' +
                                         'class="qrcode">';
                }
                scope.$emit('qrcode:error', error);
                return;
              }

              if (download) {
                domElement.download = 'qrcode.png';
                domElement.title = 'Download QR code';
              }

              if (canvas2D) {
                draw(context, qr, modules, tile);

                if (download) {
                  domElement.href = canvas.toDataURL('image/png');
                  return;
                }
              } else {
                domElement.innerHTML = qr.createImgTag(tile, 0);
                $img = element.find('img');
                $img.addClass('qrcode');

                if (download) {
                  domElement.href = $img[0].src;
                  return;
                }
              }

              if (href) {
                domElement.href = href;
              }
            };

        if (link) {
          link.className = 'qrcode-link';
          $canvas.wrap(link);
          domElement = link;
        }

        setVersion(attrs.version);
        setErrorCorrectionLevel(attrs.errorCorrectionLevel);
        setSize(attrs.size);

        attrs.$observe('version', function(value) {
          if (!value) {
            return;
          }

          setVersion(value);
          setData(data);
          setSize(size);
          render();
        });

        attrs.$observe('errorCorrectionLevel', function(value) {
          if (!value) {
            return;
          }

          setErrorCorrectionLevel(value);
          setData(data);
          setSize(size);
          render();
        });

        attrs.$observe('data', function(value) {
          if (!value) {
            return;
          }

          setData(value);
          setSize(size);
          render();
        });

        attrs.$observe('size', function(value) {
          if (!value) {
            return;
          }

          setSize(value);
          render();
        });

        attrs.$observe('href', function(value) {
          if (!value) {
            return;
          }

          href = value;
          render();
        });
      }
    };
  });

