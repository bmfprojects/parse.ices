'use strict';

var app = angular.module('ices');

app.factory("ResidentServices",  function($http, PARSE_CREDENTIALS, ResidentCollections){
var  getheaders = {
    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
    'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
    'Content-Type':'application/json'
}
return{

    CreateResident:function(data){
        return $http.post(ResidentCollections, data,{
            headers:getheaders
        });
    },

    GetAllResindet:function(){
        return $http.get(ResidentCollections,{
            headers:getheaders
        });
     },

    GetResident:function(id){
        return $http.get(ResidentCollections+id,{
            headers:getheaders
        });
    },

    UpdateResident:function(id,data){
        return $http.put(ResidentCollections+id, data,{
            headers:getheaders
        });
    },

    DeleteResident:function(id){
        return $http.delete(ResidentCollections+id,{
            headers:getheaders
        });
    }

}

});