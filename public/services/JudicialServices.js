
'use strict';

var app = angular.module('ices');

app.factory("JudicialServices",  function($http, PARSE_CREDENTIALS, JudicialCollections){
var  getheaders = {
    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
    'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
    'Content-Type':'application/json'
}
return{

    CreateJudicial:function(data){
        return $http.post(JudicialCollections, data,{
            headers:getheaders
        });
    },

    GetAllJudicial:function(){
        return $http.get(JudicialCollections,{
            headers:getheaders
        });
     },

    GetJudicial:function(id){
        return $http.get(JudicialCollections+id,{
            headers:getheaders
        });
    },

    UpdateJudicial:function(id,data){
        return $http.put(JudicialCollections+id, data,{
            headers:getheaders
        });
    },

    DeleteJudicial:function(id){
        return $http.delete(JudicialCollections+id,{
            headers:getheaders
        });
    }

}

});