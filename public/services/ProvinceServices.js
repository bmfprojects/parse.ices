'use strict';

var app = angular.module('ProvinceService', []);

app.factory("ProvinceServices",  function($q, $http, PARSE_CREDENTIALS, ProvinceCollections){
var  getheaders = {
    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
    'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
    'Content-Type':'application/json'
}

return{

    CreateProvince:function(data){
        return $http.post(ProvinceCollections, data,{
            headers:getheaders
        });
    },

    GetAllProvince:function(){
        return $http.get(ProvinceCollections,{
            headers:getheaders
        });
     },

    GetAllregionProvince:function(id){
        return $http.get(ProvinceCollections,{
            headers:getheaders
        });
     },
    GetProvince:function(id){
        return $http.get(ProvinceCollections+id,{
            headers:getheaders
        });
    },

    UpdateProvince:function(id,data){
        return $http.put(ProvinceCollections+id, data,{
            headers:getheaders
        });
    },

    DeleteProvince:function(id){
        return $http.delete(ProvinceCollections+id,{
            headers:getheaders
        });
    },

    GetProvinceByRegion:function(regiongetid) {
        var deferred = $q.defer();
        var provinces=[];
        var province = Parse.Object.extend("provinces");
        var query = new Parse.Query(province);
        query.equalTo("regiongetid", regiongetid);
        query.find({
          success: function (results) {
            for (var i = 0; i < results.length; i++) {
              var result = results[i];
              provinces.push({
                        id :  result.id,
                        province_name: result.attributes.province_name
                      });
            }
            deferred.resolve(provinces);
          },
          error: function (error) {
            deferred.reject(error);
          }
        });
        return deferred.promise;
    }

}

});