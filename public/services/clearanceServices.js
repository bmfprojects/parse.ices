'use strict';

var app = angular.module('ClearanceServices', []);

app.factory("ClearanceService",  function($http, $q, PARSE_CREDENTIALS, ClearanceCollections){
var  getheaders = {
    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
    'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
    'Content-Type':'application/json'
}
return{

    CreateClearance:function(data){
        return $http.post(ClearanceCollections, data,{
            headers:getheaders
        });
    },

    GetAllClearance:function(){
        return $http.get(ClearanceCollections,{
            headers:getheaders
        });
     },

    GetClearance:function(id){
        return $http.get(ClearanceCollections+id,{
            headers:getheaders
        });
    },

    UpdateClearance:function(id,data){
        return $http.put(ClearanceCollections+id, data,{
            headers:getheaders
        });
    },

    DeleteClearance:function(id){
        return $http.delete(ClearanceCollections+id,{
            headers:getheaders
        });
    },
    ClearanceofResident:function(id){
          var deferred = $q.defer();
          var clearancelist=[];
          var clearance = Parse.Object.extend("clearance");
          var query = new Parse.Query(clearance);
          query.equalTo("residentID", id);
          query.find({
            success: function (results) {
              for (var i = 0; i < results.length; i++) {
                var result = results[i];
                clearancelist.push({
                          id :  result.id,
                          residentclearance: result.attributes.residentclearance,
                          remark: result.attributes.remark
                        });
              }
              deferred.resolve(clearancelist);
            },
            error: function (error) {
              deferred.reject(error);
            }
          });
          return deferred.promise;
    }

}

});