'use strict';

var app = angular.module('MunicipalService', []);

app.factory("MunicipalServices",  function($http, PARSE_CREDENTIALS, MunicipalCollections){
var  getheaders = {
    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
    'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
    'Content-Type':'application/json'
}

return{

    CreateMunicipal:function(data){
        return $http.post(MunicipalCollections, data,{
            headers:getheaders
        });
    },

    GetAllMunicipal:function(){
        return $http.get(MunicipalCollections,{
            headers:getheaders
        });
     },

    GetAllprovinceMunicipal:function(id){
        return $http.get(MunicipalCollections,{
            headers:getheaders
        });
     },
    GetMunicipal:function(id){
        return $http.get(MunicipalCollections+id,{
            headers:getheaders
        });
    },

    UpdateMunicipal:function(id,data){
        return $http.put(MunicipalCollections+id, data,{
            headers:getheaders
        });
    },

    DeleteMunicipal:function(id){
        return $http.delete(MunicipalCollections+id,{
            headers:getheaders
        });
    }

}

});