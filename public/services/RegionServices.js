'use strict';

var app = angular.module('Regionservices', []);

app.factory("RegionServices",  function($http, PARSE_CREDENTIALS, RegionCollections){

var  getheaders = {
    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
    'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
    'Content-Type':'application/json'
}

return{

    CreateRegion:function(data){
        return $http.post(RegionCollections, data,{
            headers:getheaders
        });
    },

    GetAllRegion:function(){
        return $http.get(RegionCollections,{
            headers:getheaders
        });
     },

    GetRegion:function(id){
        return $http.get(RegionCollections+id,{
            headers:getheaders
        });
    },

    UpdateRegion:function(id,data){
        return $http.put(RegionCollections+id, data,{
            headers:getheaders
        });
    },

    DeleteRegion:function(id){
        return $http.delete(RegionCollections+id,{
            headers:getheaders
        });
    }

}

});