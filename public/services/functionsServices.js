'use strict';

var app = angular.module('functionsall', []);

app.factory('allPurposeFunctions', function(){

    return{

        generateage: function (dateString){

              var birthday = new Date(dateString),
              ageDifMs = Date.now() - birthday.getTime(),
              ageDate = new Date(ageDifMs), // miliseconds from epoch
              get_age = Math.abs(ageDate.getFullYear() - 1970),
              n = get_age.toString();
              return n;

        },

        generatedata: function (dateString){

            var days,
                month;

            var d = new Date(dateString);
            days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
            month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October',
            'November', 'December'];

            var today = days[d.getUTCDay()];
            var dd = days[d.getUTCDay()];
            var mm = month[d.getMonth()]; //January is 0!
            var yyyy = d.getFullYear();
            var dy = d.getDate();

            var todays = mm+' '+dy+', '+ yyyy;
            return todays;

        },

        currentTime : function(){

            var currentdate = new Date();
            var datetime = ""
                + currentdate.getHours() + ":"
                + currentdate.getMinutes() + ":"
                + currentdate.getSeconds();

            return datetime;

        },

         formatAMPM : function(date) {

              var hours = date.getHours();
              var minutes = date.getMinutes();
              var ampm = hours >= 12 ? 'pm' : 'am';
              hours = hours % 12;
              hours = hours ? hours : 12; // the hour '0' should be '12'
              minutes = minutes < 10 ? '0'+minutes : minutes;
              var strTime = hours + ':' + minutes + ' ' + ampm;
              return strTime;

        }



    }

})