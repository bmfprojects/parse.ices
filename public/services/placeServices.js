'use strict';

var app = angular.module('PlaceServices', []);

app.factory("PlaceServices",  function($q){

return{

    getprovinces: function(regionid){
          var deferred = $q.defer();
          var provinces=[];
          var province = Parse.Object.extend("provinces");
          var query = new Parse.Query(province);
          query.equalTo("region_Id", regionid);
          query.find({
            success: function (results) {
              for (var i = 0; i < results.length; i++) {
                var result = results[i];
                provinces.push({
                          id :  result.id,
                          province_name: result.attributes.province_name
                        });
              }
              deferred.resolve(provinces);
            },
            error: function (error) {
              deferred.reject(error);
            }
          });
          return deferred.promise;
        },
        getmunicipalities:function(provinceId){
          var deferred = $q.defer();
          var municipalities=[];
          var municipal = Parse.Object.extend("municipalities");
          var query = new Parse.Query(municipal);
          query.equalTo("province_Id", provinceId);
          query.find({
            success: function (results) {
              for (var i = 0; i < results.length; i++) {
                var result = results[i];
                municipalities.push({
                          id :  result.id,
                          municipality_name: result.attributes.municipality_name,
                          zip_code: result.attributes.zip_code
                        });
              }
              deferred.resolve(municipalities);
            },
            error: function (error) {
              deferred.reject(error);
            }
          });
          return deferred.promise;
        },
        getbaranggaylist:function(municipalId){
          var deferred = $q.defer();
          var baranggaylist=[];
          var baranggay = Parse.Object.extend("baranggay");
          var query = new Parse.Query(baranggay);
          query.equalTo("municipal_Id", municipalId);
          query.find({
            success: function (results) {
              for (var i = 0; i < results.length; i++) {
                var result = results[i];
                baranggaylist.push({
                          id :  result.id,
                          baranggay_name: result.attributes.baranggay_name
                        });
              }
              deferred.resolve(baranggaylist);
            },
            error: function (error) {
              deferred.reject(error);
            }
          });
          return deferred.promise;
        },
        province_items:function(regionid){
          var deferred = $q.defer();
          var provincelist=[];
          var provinces = Parse.Object.extend("provinces");
          var query = new Parse.Query(provinces);
          query.equalTo("regiongetid", regionid);
          query.find({
            success: function (results) {
              for (var i = 0; i < results.length; i++) {
                var result = results[i];
                provincelist.push({
                          id :  result.id,
                          province_name: result.attributes.province_name
                        });
              }
              deferred.resolve(provincelist);
            },
            error: function (error) {
              deferred.reject(error);
            }
          });
          return deferred.promise;
        },
        municipality_items:function(provinceid){
          var deferred = $q.defer();
          var municipality=[];
          var municipalities = Parse.Object.extend("municipalities");
          var query = new Parse.Query(municipalities);
          query.equalTo("provincegetid", provinceid);
          query.find({
            success: function (results) {
              for (var i = 0; i < results.length; i++) {
                var result = results[i];
                municipality.push({
                          id :  result.id,
                          municipality_name: result.attributes.municipality_name
                        });
              }
              deferred.resolve(municipality);
            },
            error: function (error) {
              deferred.reject(error);
            }
          });
          return deferred.promise;
        },
        baranggay_items:function(municipalid){
          var deferred = $q.defer();
          var baranggaylist=[];
          var baranggay = Parse.Object.extend("baranggay");
          var query = new Parse.Query(baranggay);
          query.equalTo("municipalgetid", municipalid);
          query.find({
            success: function (results) {
              for (var i = 0; i < results.length; i++) {
                var result = results[i];
                baranggaylist.push({
                          id :  result.id,
                          baranggay_name: result.attributes.baranggay_name
                        });
              }
              deferred.resolve(baranggaylist);
            },
            error: function (error) {
              deferred.reject(error);
            }
          });
          return deferred.promise;
        }
}

});