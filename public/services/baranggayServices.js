'use strict';

var app = angular.module('ices');

app.factory("BaranggayServices",  function($http, PARSE_CREDENTIALS, BarangayCollections){
var  getheaders = {
    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
    'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
    'Content-Type':'application/json'
}

return{

    CreateBaranggay:function(data){
        return $http.post(BarangayCollections, data,{
            headers:getheaders
        });
    },

    GetAllBaranggay:function(){
        return $http.get(BarangayCollections,{
            headers:getheaders
        });
     },

    GetAllmunicipalBaranggay:function(id){
        return $http.get(BarangayCollections,{
            headers:getheaders
        });
     },

    GetBaranggay:function(id){
        return $http.get(BarangayCollections+id,{
            headers:getheaders
        });
    },

    UpdateBaranggay:function(id,data){
        return $http.put(BarangayCollections+id, data,{
            headers:getheaders
        });
    },

    DeleteBaranggay:function(id){
        return $http.delete(BarangayCollections+id,{
            headers:getheaders
        });
    }

}

});