'use strict';

var app = angular.module('AccountService', []);

app.factory("AccountServices",  function($http, PARSE_CREDENTIALS, ViewerCollections){
var  getheaders = {
    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
    'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
    'Content-Type':'application/json'
}

return{

    CreateViewer:function(data){
        return $http.post(ViewerCollections, data,{
            headers:getheaders
        });
    },

    GetAllViewer:function(){
        return $http.get(ViewerCollections,{
            headers:getheaders
        });
     },

    GetViewer:function(id){
        return $http.get(ViewerCollections+id,{
            headers:getheaders
        });
    },

    UpdateViewer:function(id,data){
        return $http.put(ViewerCollections+id, data,{
            headers:getheaders
        });
    },

    DeleteViewer:function(id){
        return $http.delete(ViewerCollections+id,{
            headers:getheaders
        });
    }

}

});