'use strict';

var app = angular.module('UserServices', []);
app.factory('AccountService', function($http, $q, $filter, $timeout, PARSE_CREDENTIALS, UserCollections){
    var  getheaders = {
        'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
        'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
        'Content-Type':'application/json'
    }
    return{

        CreateAccount:function(data){
            return $http.post(UserCollections, data,{
                headers:getheaders
            });
        },

        GetAllAccount:function(){
            return $http.get(UserCollections,{
                headers:getheaders
            });
         },

        GetAccount:function(id){
            return $http.get(UserCollections+id,{
                headers:getheaders
            });
        },

        UpdateAccount:function(id,data){
            return $http.put(UserCollections+id, data,{
                headers:getheaders
            });
        },

        DeleteAccount:function(id){
            return $http.delete(UserCollections+id,{
                headers:getheaders
            });
        }

    }

});

app.factory('UserService', function($http, $q, $filter, $timeout, AccountService){
  var deferred = $q.defer();
  var user_result= Parse.Object.extend("_User");
  var query = new Parse.Query(user_result);

    return{

             getUserExist: function(username){
                  query.equalTo("username", username.username);
                  query.find({
                    success: function (results) {

                    if (results[0]) {
                            deferred.resolve({ success: false, message: 'Username "' + username.username+ '" is already taken' });
                        } else {
                            AccountService.CreateAccount(username);
                            deferred.resolve({ success: true, message: 'this username ' +username.username+' was added successfully' });
                        }
                    },
                    error: function (error) {
                      deferred.reject(error);
                    }
                });
                return deferred.promise;
            }

    }

})
