
  var app = angular.module('ices',
    [

        'ngRoute',
        'Parse',
        'ngAnimate',
        'ui.bootstrap',
        'ngMessages',
        'ui.select',
        'ngSanitize',
        'angularUtils.directives.dirPagination',
        'checklist-model',
        'UserServices',
        'ClearanceServices',
        'Regionservices',
        'ProvinceService',
        'MunicipalService',
        'AccountService',
        'PlaceServices',
        'functionsall',
        'angular-loading-bar'

    ]);

  app.run(function(Parse) {

    return Parse.auth.resumeSession();

  });

app.constant('PARSE_CREDENTIALS',{
    APP_ID: 'HEy9WtD8RkvSdy1N8FDrD6WVnPa9dIrjmuNmZ6Le',
    REST_API_KEY:'6WODpuL1ROZzLdWiZk59XoDu2ml5tg3hzuutGnRr'
});

app.constant('ResidentCollections', 'https://api.parse.com/1/classes/residents/');
app.constant('ClearanceCollections', 'https://api.parse.com/1/classes/clearance/');
app.constant('JudicialCollections', 'https://api.parse.com/1/classes/judicial/');
app.constant('RegionCollections', 'https://api.parse.com/1/classes/reigions/');
app.constant('ProvinceCollections', 'https://api.parse.com/1/classes/provinces/');
app.constant('MunicipalCollections', 'https://api.parse.com/1/classes/municipalities/');
app.constant('BarangayCollections', 'https://api.parse.com/1/classes/baranggay/');
app.constant('ViewerCollections', 'https://api.parse.com/1/classes/viewer/');
app.constant('UserCollections', 'https://api.parse.com/1/classes/_User/');

  app.config(function(ParseProvider, $routeProvider, $locationProvider, cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = false;
    // cfpLoadingBarProvider.includeBar = false;
    // cfpLoadingBarProvider.latencyThreshold = 500;
    ParseProvider.initialize("HEy9WtD8RkvSdy1N8FDrD6WVnPa9dIrjmuNmZ6Le", "6WODpuL1ROZzLdWiZk59XoDu2ml5tg3hzuutGnRr");
    Parse.initialize("HEy9WtD8RkvSdy1N8FDrD6WVnPa9dIrjmuNmZ6Le", "wm3D5XMFOdhh9IKQ8gz7gyJFXebfds33QQCG7AZv");

    return $routeProvider

    .when("/", {
      controller: "AuthCtrl",
      templateUrl: "views/main.html"
    })

    .when("/residentlist", {
      controller:"residentCtrl",
      templateUrl: "views/residentlist.html"
    })

    .when('/home', {
      controller: 'residentCtrl',
      templateUrl: 'views/main2.html',
      resolve:{
        // auth: function ($q, authenticationSvc) {
        //           var userInfo = authenticationSvc.getUserInfo();
        //           if (userInfo) {
        //               return $q.when(userInfo);
        //           } else {
        //               return $q.reject({ authenticated: false });
        //           }
        //       }
      }
    })

    .when('/user', {
      templateUrl: 'views/accountlist.html',
      controller: 'registerCtrl',
      resolve:{
        // auth: function ($q, authenticationSvc) {
        //           var userInfo = authenticationSvc.getUserInfo();
        //           if (userInfo) {
        //               return $q.when(userInfo);
        //           } else {
        //               return $q.reject({ authenticated: false });
        //           }
        //       }
      }
    })

    .when("/regionlist", {
      controller:"regionCtrl",
      templateUrl: "views/regions/regionlist.html"
    })

    .when("/viewprovince/:id", {
      controller:"provinceCtrl",
      templateUrl: "views/provinces/provincelist.html",
      resolve: {
          getregion: function($route, RegionServices){
            return RegionServices.GetRegion($route.current.params.id);
              }
            }

    })

    .when("/municipalities/:id", {
      controller:"municipalCtrl",
      templateUrl: "views/municipal/municipalitylist.html",
      resolve: {
          getprovince: function($route, ProvinceServices){
            return ProvinceServices.GetProvince($route.current.params.id);
              }
            }

    })

    .when("/baranggaylist/:id", {
      controller:"baranggayCtrl",
      templateUrl: "views/baranggay/baranggaylist.html",
      resolve: {
          getmunicipal: function($route, MunicipalServices){
            return MunicipalServices.GetMunicipal($route.current.params.id);
              }
            }

    })

    .when("/judicialist", {
      controller:"judicialCtrl",
      templateUrl: "views/judicialist.html"
    })

    .when("/generateIDs", {
      controller: "generateIDsCtrl",
      templateUrl: "views/generateIDs/generateIds.html",
        
    })

    .otherwise({
      redirectTo: "/"
    });

  });
$(document).ready(function(){
    $(".btn-info").click(function(){
        $(".collapse").collapse('toggle');
    });
    $("[rel='tooltip']").tooltip();
});
